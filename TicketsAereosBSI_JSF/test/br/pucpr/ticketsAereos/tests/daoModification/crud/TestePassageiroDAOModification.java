package br.pucpr.ticketsAereos.tests.daoModification.crud;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.PassageiroBC;
import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.tests.bc.crud.TestePassageiroBC;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TestePassageiroDAOModification extends TestePassageiroBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TestePassageiroDAOModification(MassaTestesPassageiroEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Metodo responsavel por inserir um passageiro na base de dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(object.getId() > 0);

		//Obtem o objeto do BD a partir do ID gerado
		Passageiro objetoBD = PassageiroBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
	}
	
	/**
	 * Metodo responsavel por atualizar um passageiro na base de dados
	 */
	@Test
	public void atualizar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		creator.update(object, "-U");
		
		//Atualiza o BD
		PassageiroBC.getInstance().update(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Passageiro objetoBD = PassageiroBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
		
		//Atualiza o BD
		PassageiroBC.getInstance().delete(object);
	}
	
	
	/**
	 * Metodo responsavel por deletar um passageiro da base de dados
	 */
	@Test
	public void deletar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Remove o objeto do BD
		PassageiroBC.getInstance().delete(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Passageiro objetoBD = PassageiroBC.getInstance().findById(object.getId());
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(objetoBD);
	}
}
