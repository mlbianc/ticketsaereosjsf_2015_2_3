package br.pucpr.ticketsAereos.tests.daoModification.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.AeroportoBC;
import br.pucpr.ticketsAereos.bc.CiaAereaBC;
import br.pucpr.ticketsAereos.bc.RotaBC;
import br.pucpr.ticketsAereos.dto.AeroportoDTO;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.bc.crud.TesteRotaBC;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;
import br.pucpr.ticketsAereos.tests.modificadores.RotaCreator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteRotaDAOModification extends TesteRotaBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteRotaDAOModification(MassaTestesRotaEnum enumm) {
		super(enumm);
	}	
	
	// ////////////////////////////////////////
	// METODOS AUXILIARES
	// ////////////////////////////////////////

	@Override
	@Before
	public void init() {
		//Inicializa o filtro com o nome da ciaAerea para pesquisar no banco de dados
		CiaAereaDTO filtro = new CiaAereaDTO();
		filtro.setNome(this.objectEnum.getCiaAerea().getNome());
		
		//Obtem a cia aerea do banco de dados
		List<CiaAerea> ciaAereas = (List<CiaAerea>) CiaAereaBC.getInstance().findByFilter(filtro);
		
		//Verifica se a lista contem elementos
		Assert.assertNotNull(ciaAereas);
		Assert.assertFalse(ciaAereas.isEmpty());
		
		//Inicializa o filtro do aeroporto
		AeroportoDTO filtroAeroporto = new AeroportoDTO();
		filtroAeroporto.setCodigo(this.objectEnum.getOrigem().getCodigo());
		
		//Obtem o aeroporto de origem do banco de dados
		List<Aeroporto> aeroportosOrigem = (List<Aeroporto>) AeroportoBC.getInstance().findByFilter(filtroAeroporto);
		
		//Verifica se a lista contem elementos
		Assert.assertNotNull(aeroportosOrigem);
		Assert.assertFalse(aeroportosOrigem.isEmpty());
		
		//Inicializa o filtro do aeroporto de destino
		filtroAeroporto.setCodigo(this.objectEnum.getDestino().getCodigo());
		
		//Obtem o aeroporto de origem do banco de dados
		List<Aeroporto> aeroportosDestino = (List<Aeroporto>) AeroportoBC.getInstance().findByFilter(filtroAeroporto);

		//Verifica se a lista contem elementos
		Assert.assertNotNull(aeroportosDestino);
		Assert.assertFalse(aeroportosDestino.isEmpty());
		
		//Obtem o objeto Rota com as informacoes do banco de dados
		this.object = ((RotaCreator)this.creator).create(this.objectEnum, ciaAereas.get(0), aeroportosOrigem.get(0), aeroportosDestino.get(0));
	}	
	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Metodo responsavel por inserir uma rota na base de dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(object.getId() > 0);

		//Obtem o objeto do BD a partir do ID gerado
		Rota objetoBD = RotaBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
	}
	
	/**
	 * Metodo responsavel por atualizar uma rota na base de dados
	 */
	@Test
	public void atualizar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		creator.update(object, "-U");
		
		//Atualiza o BD
		RotaBC.getInstance().update(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Rota objetoBD = RotaBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
		
		//Atualiza o BD
		RotaBC.getInstance().delete(object);
	}
	
	
	/**
	 * Metodo responsavel por deletar uma rota da base de dados
	 */
	@Test
	public void deletar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Remove o objeto do BD
		RotaBC.getInstance().delete(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Rota objetoBD = RotaBC.getInstance().findById(object.getId());
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(objetoBD);
	}
}
