package br.pucpr.ticketsAereos.tests.daoModification.crud;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import br.pucpr.ticketsAereos.tests.daoQueries.crud.TesteAeroportoDAOQueries;
import br.pucpr.ticketsAereos.tests.daoQueries.crud.TesteCiaAereaDAOQueries;
import br.pucpr.ticketsAereos.tests.daoQueries.crud.TesteFuncionarioDAOQueries;
import br.pucpr.ticketsAereos.tests.daoQueries.crud.TestePassageiroDAOQueries;
import br.pucpr.ticketsAereos.tests.daoQueries.crud.TesteRotaDAOQueries;

/**
 * Essa classe realizara os testes da camada DAO de uma so vez.
 * Para que tudo ocorra com sucesso, a base devera estar vazia, pois existem metodos no
 * pacote Queries que necessitam que o numero de instancias da classe X esteja igual ao
 * numero descrito no Enum de testes da classe X.
 * Para limpar a base basta deletar os arquivos que se encontram na pasta banco e executar
 * novamente o script do banco de dados.
 * 
 * @author Mauda
 *
 */

public class RunnerTestes {
	
	private JUnitCore junit = new JUnitCore();
	
	private Results daoModification = new Results();
	private Results daoQueries = new Results();
	
	class Results{
		private Integer qtdTotal = 0;
		private Integer qtdErros = 0;
		private Integer qtdFailures = 0;
		
		public void report(String title){
			System.out.println("======================================================");
			System.out.println(title);
			System.out.println("Testes Totais: " + qtdTotal);
			System.out.println("Testes Success (Verdes): " + qtdSuccess());
			System.out.println("Testes Failures (Azuis): " + qtdFailures);
			System.out.println("Testes Errors (Vermelhos): " + qtdErros);
			System.out.println("======================================================\n\n");
		}
		
		public Integer qtdSuccess(){
			return qtdTotal - qtdErros - qtdFailures;
		}
	}
	
	@Test
	public void runner(){
		
		//Runner para a classe TestePassageiroDAOModification
		run(TestePassageiroDAOModification.class, daoModification);

		//Runner para a classe TesteAeroportoDAOModification
		run(TesteAeroportoDAOModification.class, daoModification);
		
		//Runner para a classe TesteCiaAereaDAOModification
		run(TesteCiaAereaDAOModification.class, daoModification);
		
		//Runner para a classe TesteFuncionarioDAOModification
		run(TesteFuncionarioDAOModification.class, daoModification);
		
		//Runner para a classe TesteRotaDAOModification
		run(TesteRotaDAOModification.class, daoModification);
		
		daoModification.report("RELATORIO DOS TESTES DAO MODIFICATION");
		
		
		
		//Runner para a classe TestePassageiroDAOQueries
		run(TestePassageiroDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteAeroportoDAOQueries
		run(TesteAeroportoDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteCiaAereaDAOQueries
		run(TesteCiaAereaDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteFuncionarioDAOQueries
		run(TesteFuncionarioDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteRotaDAOQueries
		run(TesteRotaDAOQueries.class, daoQueries);
		
		daoQueries.report("RELATORIO DOS TESTES DAO QUERIES");
		
		
		
		
		System.out.println("======================================================");
		System.out.println("RELATORIO FINAL DOS TESTES DAO ");
		System.out.println("Testes Totais: " + (daoModification.qtdTotal + daoQueries.qtdTotal));
		System.out.println("Testes Success (Verdes): " + (daoModification.qtdSuccess() + daoQueries.qtdSuccess()));
		System.out.println("Testes Failures (Azuis): " + (daoModification.qtdFailures + daoQueries.qtdFailures));
		System.out.println("Testes Errors (Vermelhos): " + (daoModification.qtdErros + daoQueries.qtdErros));
		System.out.println("======================================================\n\n");
		
	}
	
	private void run(Class<?> classesTeste, Results results){
		Result r = junit.run(classesTeste);
		results.qtdTotal += r.getRunCount();
		for(Failure fail : r.getFailures()){
			if(fail.getException() instanceof AssertionError){
				results.qtdFailures++;
			} else {
				results.qtdErros++;
			}
		}
	}
	
}
