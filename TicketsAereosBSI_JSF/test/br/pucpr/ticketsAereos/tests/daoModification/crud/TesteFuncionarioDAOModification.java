package br.pucpr.ticketsAereos.tests.daoModification.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.CiaAereaBC;
import br.pucpr.ticketsAereos.bc.FuncionarioBC;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.model.Funcionario;
import br.pucpr.ticketsAereos.tests.bc.crud.TesteFuncionarioBC;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.tests.modificadores.FuncionarioCreator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteFuncionarioDAOModification extends TesteFuncionarioBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteFuncionarioDAOModification(MassaTestesFuncionarioEnum enumm) {
		super(enumm);
	}	
	
	// ////////////////////////////////////////
	// METODOS AUXILIARES
	// ////////////////////////////////////////

	@Override
	@Before
	public void init() {
		//Inicializa o filtro com o nome da ciaAerea para pesquisar no banco de dados
		CiaAereaDTO filtro = new CiaAereaDTO();
		filtro.setNome(this.objectEnum.getCiaAerea().getNome());
		
		//Obtem a cia aerea do banco de dados
		List<CiaAerea> ciaAereas = (List<CiaAerea>) CiaAereaBC.getInstance().findByFilter(filtro);
		
		//Verifica se a lista contem elementos
		Assert.assertNotNull(ciaAereas);
		Assert.assertFalse(ciaAereas.isEmpty());
		
		this.object = ((FuncionarioCreator)this.creator).create(this.objectEnum, ciaAereas.get(0));
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Metodo responsavel por inserir um funcionario na base de dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(this.object.getId() > 0);

		//Obtem o objeto do BD a partir do ID gerado
		Funcionario objetoBD = FuncionarioBC.getInstance().findById(this.object.getId());
		
		//Realiza as verificacoes
		this.verificator.verify(objetoBD, this.object);
	}
	
	/**
	 * Metodo responsavel por atualizar um funcionario na base de dados
	 */
	@Test
	public void atualizar(){
		//Realiza o INSERT na chamada do metodo para o BC
		this.criar();
		
		//Atualiza Atributos
		this.creator.update(this.object, "-U");
		
		//Atualiza o BD
		FuncionarioBC.getInstance().update(this.object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Funcionario objetoBD = FuncionarioBC.getInstance().findById(this.object.getId());
		
		//Realiza as verificacoes
		this.verificator.verify(objetoBD, this.object);
		
		//Atualiza o BD
		FuncionarioBC.getInstance().delete(this.object);
	}
	
	
	/**
	 * Metodo responsavel por deletar um funcionario da base de dados
	 */
	@Test
	public void deletar(){
		//Realiza o INSERT na chamada do metodo para o BC
		this.criar();
		
		//Remove o objeto do BD
		FuncionarioBC.getInstance().delete(this.object);
		
		//Obtem o objeto do BD a partir do ID gerado
		Funcionario objetoBD = FuncionarioBC.getInstance().findById(this.object.getId());
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(objetoBD);
	}
}
