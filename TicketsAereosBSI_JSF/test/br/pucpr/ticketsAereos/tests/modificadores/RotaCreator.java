package br.pucpr.ticketsAereos.tests.modificadores;

import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;

public class RotaCreator extends Creator<Rota, MassaTestesRotaEnum> {

	private static RotaCreator instance = new RotaCreator();
	
	private RotaCreator() {	}
	
	@Override
	public Rota createBlank() {
		return new Rota(CiaAereaCreator.getInstance().createBlank(), 
						AeroportoCreator.getInstance().createBlank(), 
						AeroportoCreator.getInstance().createBlank());
	}
	
	@Override
	public Rota create(MassaTestesRotaEnum enumm) {
		//Cria a ciaAerea
		CiaAerea ciaAerea = CiaAereaCreator.getInstance().create(enumm.getCiaAerea());
		
		//Cria o aeroporto de origem
		Aeroporto origem = AeroportoCreator.getInstance().create(enumm.getOrigem());
		
		//Cria o aeroporto de destino
		Aeroporto destino = AeroportoCreator.getInstance().create(enumm.getDestino());
		
		//Cria o aviao
		Rota rota = new Rota(ciaAerea, origem, destino);
		
		//Atualiza as informacoes de acordo com o enum
		this.update(rota, enumm);
		
		//Retorna o aeroporto
		return rota;
	}
	
	public Rota create(MassaTestesRotaEnum enumm, CiaAerea ciaAerea, Aeroporto origem, Aeroporto destino) {
		//Cria o aviao
		Rota rota = new Rota(ciaAerea, origem, destino);
		
		//Atualiza as informacoes de acordo com o enum
		this.update(rota, enumm);
		
		//Retorna o aeroporto
		return rota;
	}	

	@Override
	public void update(Rota rota, MassaTestesRotaEnum enumm) {
		super.update(rota, enumm);
		
		rota.setDescricao(enumm.getDescricao());
		rota.setNome(enumm.getNome());
	}
	
	@Override
	public void update(Rota rota, String codigo){
		rota.setDescricao(codigo + rota.getDescricao());
		rota.setNome(codigo + rota.getNome());
	}
	
	public static RotaCreator getInstance() {
		return instance;
	}
}
