package br.pucpr.ticketsAereos.tests.modificadores;

import java.util.Date;

import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.model.Funcionario;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

public class FuncionarioCreator extends Creator<Funcionario, MassaTestesFuncionarioEnum> {

	private static FuncionarioCreator instance = new FuncionarioCreator();
	
	private FuncionarioCreator() {	}
	
	@Override
	public Funcionario createBlank() {
		return new Funcionario(EnderecoCreator.getInstance().createBlank(), CiaAereaCreator.getInstance().createBlank());
	}
	
	@Override
	public Funcionario create(MassaTestesFuncionarioEnum enumm) {
		//Cria a CiaAerea
		CiaAerea ciaAerea = CiaAereaCreator.getInstance().create(enumm.getCiaAerea());
		
		return this.create(enumm, ciaAerea);
	}
	
	public Funcionario create(MassaTestesFuncionarioEnum enumm, CiaAerea ciaAerea) {
		//Cria o endereco
		Endereco endereco = EnderecoCreator.getInstance().create(enumm.getEnderecoEnum());
		
		//Cria o funcionario
		Funcionario funcionario = new Funcionario(endereco, ciaAerea);
		
		//Atualiza as informacoes de acordo com o enum
		this.update(funcionario, enumm);
		
		//Retorna a pessoa
		return funcionario;
	}
	

	@Override
	public void update(Funcionario funcionario, MassaTestesFuncionarioEnum enumm) {
		super.update(funcionario, enumm);
		
		funcionario.setNome(enumm.getNome());
		funcionario.setEmail(enumm.getEmail());
		funcionario.setTelefone(enumm.getTelefone());
		funcionario.setDataNascimento(enumm.getDataNascimento());
		
		EnderecoCreator.getInstance().update(funcionario.getEndereco(), enumm.getEnderecoEnum());
		
		funcionario.setCodigo(enumm.getCodigo());
		funcionario.setContaCorrente(enumm.getContaCorrente());
	}
	
	@Override
	public void update(Funcionario funcionario, String codigo){
		funcionario.setEmail(codigo 		+ funcionario.getEmail());
		funcionario.setNome(codigo 		+ funcionario.getNome());
		funcionario.setTelefone(codigo 	+ funcionario.getTelefone());
		funcionario.setDataNascimento(	DateUtils.minimizeDate(new Date()));

		//Atualiza o endereco
		EnderecoCreator.getInstance().update(funcionario.getEndereco(), codigo);
		
		funcionario.setCodigo(codigo		+ funcionario.getCodigo());
		funcionario.setContaCorrente(codigo	+ funcionario.getContaCorrente());
	}
	
	public static FuncionarioCreator getInstance() {
		return instance;
	}
}
