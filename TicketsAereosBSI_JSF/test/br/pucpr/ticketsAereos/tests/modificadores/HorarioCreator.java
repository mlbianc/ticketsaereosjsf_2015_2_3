package br.pucpr.ticketsAereos.tests.modificadores;

import br.pucpr.ticketsAereos.model.Horario;
import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

public class HorarioCreator extends Creator<Horario, MassaTestesHorarioEnum> {

	private static HorarioCreator instance = new HorarioCreator();
	
	private HorarioCreator() {	}
	
	@Override
	public Horario createBlank() {
		return new Horario(RotaCreator.getInstance().createBlank(), 1, 1, 1);
	}
	
	@Override
	public Horario create(MassaTestesHorarioEnum enumm) {
		//Cria a rota
		Rota rota = RotaCreator.getInstance().create(enumm.getRota());
		
		return this.create(enumm, rota);		
	}

	public Horario create(MassaTestesHorarioEnum enumm, Rota rota) {
		//Cria o horario
		Horario horario = new Horario(rota, enumm.getQtdEconomica(), enumm.getQtdExecutiva(), enumm.getQtdPrimeira());
		
		//Atualiza as informacoes de acordo com o enum
		this.update(horario, enumm);
		
		return horario;		
	}
	
	@Override
	public void update(Horario horario, MassaTestesHorarioEnum enumm) {
		super.update(horario, enumm);
		
		horario.setPartida(enumm.getDataPartida());
		horario.setChegada(enumm.getDataChegada());
		horario.setCodigo(enumm.getCodigo());
		horario.setQtdEconomica(enumm.getQtdEconomica());
		horario.setQtdExecutiva(enumm.getQtdExecutiva());
		horario.setQtdPrimeira(enumm.getQtdPrimeira());
	}
	
	@Override
	public void update(Horario horario, String codigo){
		horario.setPartida(DateUtils.datePlusDays(horario.getPartida(), 10));
		horario.setChegada(DateUtils.datePlusDays(horario.getChegada(), 10));
		
		horario.setCodigo(codigo 		+ horario.getCodigo());
	}
	
	public static HorarioCreator getInstance() {
		return instance;
	}

}
