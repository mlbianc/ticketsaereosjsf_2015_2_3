package br.pucpr.ticketsAereos.tests.modificadores;

import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;

public class AeroportoCreator extends Creator<Aeroporto, MassaTestesAeroportoEnum> {

	private static AeroportoCreator instance = new AeroportoCreator();
	
	private AeroportoCreator() {	}
	
	@Override
	public Aeroporto createBlank() {
		return new Aeroporto(EnderecoCreator.getInstance().createBlank());
	}
	
	@Override
	public Aeroporto create(MassaTestesAeroportoEnum enumm) {
		//Cria o endereco
		Endereco endereco = EnderecoCreator.getInstance().create(enumm.getEnderecoEnum());
				
		//Cria o aeroporto
		Aeroporto aeroporto = new Aeroporto(endereco);
		
		//Atualiza as informacoes de acordo com o enum
		this.update(aeroporto, enumm);
		
		//Retorna o aeroporto
		return aeroporto;
	}

	@Override
	public void update(Aeroporto aeroporto, MassaTestesAeroportoEnum enumm) {
		super.update(aeroporto, enumm);
		
		aeroporto.setNome(enumm.getNome());
		aeroporto.setCodigo(enumm.getCodigo());
		
		EnderecoCreator.getInstance().update(aeroporto.getEndereco(), enumm.getEnderecoEnum());
	}
	
	@Override
	public void update(Aeroporto aeroporto, String codigo){
		aeroporto.setCodigo(codigo 		+ aeroporto.getCodigo());
		aeroporto.setNome(codigo 		+ aeroporto.getNome());

		//Atualiza o endereco
		EnderecoCreator.getInstance().update(aeroporto.getEndereco(), codigo);
	}
	
	public static AeroportoCreator getInstance() {
		return instance;
	}
}
