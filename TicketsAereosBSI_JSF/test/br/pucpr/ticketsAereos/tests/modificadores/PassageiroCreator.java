package br.pucpr.ticketsAereos.tests.modificadores;

import java.util.Date;

import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

public class PassageiroCreator extends Creator<Passageiro, MassaTestesPassageiroEnum> {

	private static PassageiroCreator instance = new PassageiroCreator();
	
	private PassageiroCreator() {	}
	
	@Override
	public Passageiro createBlank() {
		return new Passageiro(EnderecoCreator.getInstance().createBlank());
	}
	
	@Override
	public Passageiro create(MassaTestesPassageiroEnum enumm) {
		//Cria o endereco
		Endereco endereco = EnderecoCreator.getInstance().create(enumm.getEnderecoEnum());
		
		//Cria o passageiro
		Passageiro passageiro = new Passageiro(endereco);
		
		//Atualiza as informacoes de acordo com o enum
		this.update(passageiro, enumm);
		
		//Retorna a pessoa
		return passageiro;
	}

	@Override
	public void update(Passageiro passageiro, MassaTestesPassageiroEnum enumm) {
		super.update(passageiro, enumm);
		
		passageiro.setNome(enumm.getNome());
		passageiro.setEmail(enumm.getEmail());
		passageiro.setTelefone(enumm.getTelefone());
		passageiro.setDataNascimento(enumm.getDataNascimento());
		
		EnderecoCreator.getInstance().update(passageiro.getEndereco(), enumm.getEnderecoEnum());
		
		passageiro.setDocumento(enumm.getDocumento());
		passageiro.setNumeroCartao(enumm.getNumeroCartao());
	}
	
	@Override
	public void update(Passageiro passageiro, String codigo){
		passageiro.setEmail(codigo 		+ passageiro.getEmail());
		passageiro.setNome(codigo 		+ passageiro.getNome());
		passageiro.setTelefone(codigo 	+ passageiro.getTelefone());
		passageiro.setDataNascimento(	DateUtils.minimizeDate(new Date()));
		
		//Atualiza o endereco
		EnderecoCreator.getInstance().update(passageiro.getEndereco(), codigo);		
		passageiro.setDocumento(codigo		+ passageiro.getDocumento());
		passageiro.setNumeroCartao(codigo	+ passageiro.getNumeroCartao());
	}
	
	public static PassageiroCreator getInstance() {
		return instance;
	}
}
