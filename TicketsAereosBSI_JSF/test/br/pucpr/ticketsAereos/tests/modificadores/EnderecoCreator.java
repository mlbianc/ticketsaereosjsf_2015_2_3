package br.pucpr.ticketsAereos.tests.modificadores;

import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;

public class EnderecoCreator extends Creator<Endereco, MassaTestesEnderecoEnum> {

	private static EnderecoCreator instance = new EnderecoCreator();
	
	private EnderecoCreator() {	}
	
	@Override
	public Endereco createBlank() {
		return new Endereco();
	}
	
	@Override
	public Endereco create(MassaTestesEnderecoEnum enumm) {
		//Cria o endereco
		Endereco endereco = new Endereco();
		
		//Atualiza as informacoes de acordo com o enum
		this.update(endereco, enumm);
		
		//Retorna o endereco
		return endereco;
	}

	@Override
	public void update(Endereco endereco, MassaTestesEnderecoEnum enumm) {
		super.update(endereco, enumm);
		
		endereco.setBairro(enumm.getBairro());
		endereco.setCidade(enumm.getCidade());
		endereco.setComplemento(enumm.getComplemento());
		endereco.setEstado(enumm.getEstado());
		endereco.setNumero(enumm.getNumero());
		endereco.setPais(enumm.getPais());
		endereco.setRua(enumm.getRua());
	}
	
	@Override
	public void update(Endereco endereco, String codigo) {
		endereco.setBairro(codigo 		+ endereco.getBairro());
		endereco.setCidade(codigo 		+ endereco.getCidade());
		endereco.setComplemento(codigo 	+ endereco.getComplemento());
		endereco.setEstado(codigo 		+ endereco.getEstado());
		endereco.setNumero(				100000);
		endereco.setPais(codigo 		+ endereco.getPais());
		endereco.setRua(codigo 			+ endereco.getRua());
	}	
	
	public static EnderecoCreator getInstance() {
		return instance;
	}

}
