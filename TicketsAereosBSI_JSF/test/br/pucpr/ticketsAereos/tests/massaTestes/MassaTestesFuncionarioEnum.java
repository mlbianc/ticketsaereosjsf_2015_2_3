package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_BELO_HORIZONTE;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_BRASILIA;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_CURITIBA;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import br.pucpr.ticketsAereos.utils.DateUtils;

public enum MassaTestesFuncionarioEnum{
	
	FUNCIONARIO_01('F', "Funcionario 01", "funcionario01@funcionarios.com.br", "f11111111", "09/03/1987", 
			"f345", "12121212", END_BRASILIA, MassaTestesCiaAereaEnum.AZUL),
			
	FUNCIONARIO_02('F', "Funcionario 02", "funcionario02@funcionarios.com.br", "f22222222", "07/05/1960", 
			"f123", "56565656", END_BELO_HORIZONTE, MassaTestesCiaAereaEnum.AZUL),
			
	FUNCIONARIO_03('F', "Funcionario 03", "funcionario03@funcionarios.com.br", "f33333333", "20/07/1984", 
			"f789", "34343434", END_CURITIBA, MassaTestesCiaAereaEnum.AZUL);
	
	private char tipo;
	private String nome, email, telefone, codigo, contaCorrente;
	private Date dataNascimento;
	private MassaTestesEnderecoEnum enderecoEnum;
	private MassaTestesCiaAereaEnum ciaAereaEnum;
	
	private MassaTestesFuncionarioEnum(char tipo, String nome, String email, String telefone, String dataNascimento,  
				String codigo, String contaCorrente, MassaTestesEnderecoEnum enderecoEnum, 
				MassaTestesCiaAereaEnum ciaAereaEnum) {
		this.tipo = tipo;
		this.dataNascimento = DateUtils.converterDate(dataNascimento);
		this.email = email;
		this.nome = nome;
		this.telefone = telefone;
		this.enderecoEnum = enderecoEnum;
		this.codigo = codigo;
		this.contaCorrente = contaCorrente;
		this.ciaAereaEnum = ciaAereaEnum;
	}
	
	public char getTipo() {
		return this.tipo;
	}
	
	public Date getDataNascimento() {
		return this.dataNascimento;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getTelefone() {
		return this.telefone;
	}
	
	public String getCodigo() {
		return this.codigo;
	}
	
	public String getContaCorrente() {
		return this.contaCorrente;
	}
	
	public MassaTestesEnderecoEnum getEnderecoEnum() {
		return this.enderecoEnum;
	}
	
	public MassaTestesCiaAereaEnum getCiaAerea() {
		return this.ciaAereaEnum;
	}
	
	public static MassaTestesFuncionarioEnum getInstance(){
		Random generator = new Random();
		MassaTestesFuncionarioEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesFuncionarioEnum[]> getParameters(){
		Collection<MassaTestesFuncionarioEnum[]> c = new ArrayList<MassaTestesFuncionarioEnum[]>();
		for(MassaTestesFuncionarioEnum ciaAerea : values()){
			c.add(new MassaTestesFuncionarioEnum[] { ciaAerea });
		}
		return c;
	}		
}
