package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_MAO_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_MAO_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_MAO_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_NACIONAL_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_NACIONAL_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum.BAGAGEM_NACIONAL_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_01;

import java.util.ArrayList;
import java.util.Collection;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;


public enum MassaTestesCheckInBilheteEnum {
	
	
	CHECKIN_PASSAGEIRO_01_BILHETE_01_BAGAGEM_01(PASSAGEIRO_01_BILHETE_01, BAGAGEM_MAO_01),
	
	CHECKIN_PASSAGEIRO_02_BILHETE_01_BAGAGEM_01(PASSAGEIRO_02_BILHETE_01, BAGAGEM_MAO_02),
	CHECKIN_PASSAGEIRO_02_BILHETE_01_BAGAGEM_02(PASSAGEIRO_02_BILHETE_01, BAGAGEM_NACIONAL_01),
	
	CHECKIN_PASSAGEIRO_03_BILHETE_01_BAGAGEM_01(PASSAGEIRO_03_BILHETE_01, BAGAGEM_MAO_03),
	CHECKIN_PASSAGEIRO_03_BILHETE_01_BAGAGEM_02(PASSAGEIRO_03_BILHETE_01, BAGAGEM_NACIONAL_02),
	CHECKIN_PASSAGEIRO_03_BILHETE_01_BAGAGEM_03(PASSAGEIRO_03_BILHETE_01, BAGAGEM_NACIONAL_03);

	private MassaTestesBilheteEnum bilheteEnum;
	private MassaTestesBagagemEnum bagagemEnum;
	
	private MassaTestesCheckInBilheteEnum(MassaTestesBilheteEnum bilheteEnum, MassaTestesBagagemEnum bagagemEnum) {
		this.bilheteEnum = bilheteEnum;
		this.bagagemEnum = bagagemEnum;
	}
	
	public MassaTestesBilheteEnum getBilheteEnum() {
		return this.bilheteEnum;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.CHECKIN;
	}
	
	public MassaTestesBagagemEnum getBagagemEnum() {
		return this.bagagemEnum;
	}
	
	public static Collection<MassaTestesCheckInBilheteEnum[]> getParameters(){
		Collection<MassaTestesCheckInBilheteEnum[]> c = new ArrayList<MassaTestesCheckInBilheteEnum[]>();
		for(MassaTestesCheckInBilheteEnum ciaAerea : values()){
			c.add(new MassaTestesCheckInBilheteEnum[] { ciaAerea });
		}
		return c;
	}
}
