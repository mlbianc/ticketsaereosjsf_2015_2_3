package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_ECONOMICA_2;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_ECONOMICA_3;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_ECONOMICA_5;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_EXECUTIVA_2;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_EXECUTIVA_3;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_EXECUTIVA_5;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_PRIMEIRA_2;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_PRIMEIRA_3;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.QTD_PRIMEIRA_5;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_BSB_CWB;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_BSB_SDU;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_CWB_GRU;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_GRU_CWB;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_GRU_SDU;
import static br.pucpr.ticketsAereos.tests.massaTestes.ConstantesPopulate.TEMPO_VOO_SDU_BSB;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_BSB_CWB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_BSB_SDU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_CWB_GRU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_CWB_GRU_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_GRU_CWB_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_GRU_SDU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum.ROTA_SDU_BSB_AZUL;
import static br.pucpr.ticketsAereos.utils.DateUtils.currentDatePlusDays;
import static br.pucpr.ticketsAereos.utils.DateUtils.datePlusHours;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

public enum MassaTestesHorarioEnum {
	
	/////////////////////////////////////////
	// BRASILIA - AZUL
	/////////////////////////////////////////
	
	HOR_DIA_1_BSB_CWB_AZUL("BSB-CWB-Azul-1", currentDatePlusDays(1), TEMPO_VOO_BSB_CWB,
			QTD_ECONOMICA_2, QTD_EXECUTIVA_2, QTD_PRIMEIRA_2, ROTA_BSB_CWB_AZUL),
	HOR_DIA_5_BSB_CWB_AZUL("BSB-CWB-Azul-5", currentDatePlusDays(5), TEMPO_VOO_BSB_CWB,
			QTD_ECONOMICA_2, QTD_EXECUTIVA_2, QTD_PRIMEIRA_2, ROTA_BSB_CWB_AZUL),
	HOR_DIA_6_BSB_CWB_AZUL("BSB-CWB-Azul-6", currentDatePlusDays(6), TEMPO_VOO_BSB_CWB,
			QTD_ECONOMICA_2, QTD_EXECUTIVA_2, QTD_PRIMEIRA_2, ROTA_BSB_CWB_AZUL),	

	HOR_DIA_4_BSB_SDU_AZUL("BSB-SDU-Azul-4", currentDatePlusDays(4), TEMPO_VOO_BSB_SDU,
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_BSB_SDU_AZUL),
	
	/////////////////////////////////////////
	// CURITIBA - AZUL
	/////////////////////////////////////////
			
	HOR_DIA_1_CWB_GRU_AZUL("CWB-GRU-Azul-1", currentDatePlusDays(1), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_AZUL),
	HOR_DIA_2_CWB_GRU_AZUL("CWB-GRU-Azul-2", currentDatePlusDays(2), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_AZUL),	
	HOR_DIA_6_CWB_GRU_AZUL("CWB-GRU-Azul-6", currentDatePlusDays(6), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_AZUL),
			
	/////////////////////////////////////////
	// GUARULHOS - AZUL
	/////////////////////////////////////////

	HOR_DIA_2_GRU_SDU_AZUL("GRU-SDU-Azul-2", currentDatePlusDays(2), TEMPO_VOO_GRU_SDU,
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_GRU_SDU_AZUL),			
	HOR_DIA_3_GRU_SDU_AZUL("GRU-SDU-Azul-3", currentDatePlusDays(3), TEMPO_VOO_GRU_SDU,
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_GRU_SDU_AZUL),
			
	
	/////////////////////////////////////////
	// RIO DE JANEIRO - AZUL
	/////////////////////////////////////////
	
	HOR_DIA_3_SDU_BSB_AZUL("SDU-BSB-Azul-3", currentDatePlusDays(3), TEMPO_VOO_SDU_BSB, 
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_SDU_BSB_AZUL),	
	HOR_DIA_4_SDU_BSB_AZUL("SDU-BSB-Azul-4", currentDatePlusDays(4), TEMPO_VOO_SDU_BSB, 
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_SDU_BSB_AZUL),	
	HOR_DIA_5_SDU_BSB_AZUL("SDU-BSB-Azul-5", currentDatePlusDays(5), TEMPO_VOO_SDU_BSB, 
			QTD_ECONOMICA_5, QTD_EXECUTIVA_5, QTD_PRIMEIRA_5, ROTA_SDU_BSB_AZUL),	
			
			
	/////////////////////////////////////////
	// CURITIBA - GOL
	/////////////////////////////////////////

	HOR_DIA_1_CWB_GRU_GOL("CWB-GRU-Gol-1", currentDatePlusDays(1), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_GOL),			
	HOR_DIA_3_CWB_GRU_GOL("CWB-GRU-Gol-3", currentDatePlusDays(3), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_GOL),
	HOR_DIA_5_CWB_GRU_GOL("CWB-GRU-Gol-5", currentDatePlusDays(5), TEMPO_VOO_CWB_GRU, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_CWB_GRU_GOL),
			
	/////////////////////////////////////////
	// GUARULHOS - GOL
	/////////////////////////////////////////

	HOR_DIA_2_GRU_CWB_GOL("GRU-CWB-Gol-2", currentDatePlusDays(2), TEMPO_VOO_GRU_CWB, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_GRU_CWB_GOL),			
	HOR_DIA_4_GRU_CWB_GOL("GRU-CWB-Gol-4", currentDatePlusDays(4), TEMPO_VOO_GRU_CWB, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_GRU_CWB_GOL),
	HOR_DIA_6_GRU_CWB_GOL("GRU-CWB-Gol-6", currentDatePlusDays(6), TEMPO_VOO_GRU_CWB, 
			QTD_ECONOMICA_3, QTD_EXECUTIVA_3, QTD_PRIMEIRA_3, ROTA_GRU_CWB_GOL);
	
	private String codigo;
	private Date dataPartida, dataChegada;
	private int tempoViagem, qtdEconomica, qtdExecutiva, qtdPrimeira;
	private MassaTestesRotaEnum rota;
	
	
	private MassaTestesHorarioEnum(String codigo, Date dataPartida, int tempoViagem, int qtdEconomica, int qtdExecutiva, 
			int qtdPrimeira, MassaTestesRotaEnum rotaEnum) {
		this.codigo = codigo;
		this.dataPartida = dataPartida;
		this.dataChegada = datePlusHours(dataPartida, tempoViagem);
		this.tempoViagem = tempoViagem;
		this.qtdEconomica = qtdEconomica;
		this.qtdExecutiva = qtdExecutiva;
		this.qtdPrimeira = qtdPrimeira;
		this.rota = rotaEnum;
	}
	
	public String getCodigo() {
		return this.codigo;
	}
	
	public Date getDataPartida() {
		return this.dataPartida;
	}
	
	public Date getDataChegada() {
		return this.dataChegada;
	}
	
	public int getTempoViagem() {
		return this.tempoViagem;
	}
	
	public int getQtdEconomica() {
		return this.qtdEconomica;
	}
	
	public int getQtdExecutiva() {
		return this.qtdExecutiva;
	}
	
	public int getQtdPrimeira() {
		return this.qtdPrimeira;
	}
	
	public MassaTestesRotaEnum getRota() {
		return this.rota;
	}
	
	public static MassaTestesHorarioEnum getInstance(){
		Random generator = new Random();
		MassaTestesHorarioEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesHorarioEnum[]> getParameters(){
		Collection<MassaTestesHorarioEnum[]> c = new ArrayList<MassaTestesHorarioEnum[]>();
		for(MassaTestesHorarioEnum ciaAerea : values()){
			c.add(new MassaTestesHorarioEnum[] { ciaAerea });
		}
		return c;
	}	
	
}
