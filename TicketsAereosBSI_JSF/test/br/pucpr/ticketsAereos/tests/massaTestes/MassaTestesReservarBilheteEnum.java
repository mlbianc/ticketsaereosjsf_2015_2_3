package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_04;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_05;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_06;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_04;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_05;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_06;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_04;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_05;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_06;

import java.util.ArrayList;
import java.util.Collection;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;


public enum MassaTestesReservarBilheteEnum {
	
	RESERVAR_PASSAGEIRO_01_BILHETE_01(PASSAGEIRO_01_BILHETE_01),
	RESERVAR_PASSAGEIRO_01_BILHETE_02(PASSAGEIRO_01_BILHETE_02),
	RESERVAR_PASSAGEIRO_01_BILHETE_03(PASSAGEIRO_01_BILHETE_03),
	RESERVAR_PASSAGEIRO_01_BILHETE_04(PASSAGEIRO_01_BILHETE_04),
	RESERVAR_PASSAGEIRO_01_BILHETE_05(PASSAGEIRO_01_BILHETE_05),
	RESERVAR_PASSAGEIRO_01_BILHETE_06(PASSAGEIRO_01_BILHETE_06),
	
	RESERVAR_PASSAGEIRO_02_BILHETE_01(PASSAGEIRO_02_BILHETE_01),
	RESERVAR_PASSAGEIRO_02_BILHETE_02(PASSAGEIRO_02_BILHETE_02),
	RESERVAR_PASSAGEIRO_02_BILHETE_03(PASSAGEIRO_02_BILHETE_03),
	RESERVAR_PASSAGEIRO_02_BILHETE_04(PASSAGEIRO_02_BILHETE_04),
	RESERVAR_PASSAGEIRO_02_BILHETE_05(PASSAGEIRO_02_BILHETE_05),
	RESERVAR_PASSAGEIRO_02_BILHETE_06(PASSAGEIRO_02_BILHETE_06),

	RESERVAR_PASSAGEIRO_03_BILHETE_01(PASSAGEIRO_03_BILHETE_01),
	RESERVAR_PASSAGEIRO_03_BILHETE_02(PASSAGEIRO_03_BILHETE_02),
	RESERVAR_PASSAGEIRO_03_BILHETE_03(PASSAGEIRO_03_BILHETE_03),
	RESERVAR_PASSAGEIRO_03_BILHETE_04(PASSAGEIRO_03_BILHETE_04),
	RESERVAR_PASSAGEIRO_03_BILHETE_05(PASSAGEIRO_03_BILHETE_05),
	RESERVAR_PASSAGEIRO_03_BILHETE_06(PASSAGEIRO_03_BILHETE_06);
	
	private MassaTestesBilheteEnum bilheteEnum;
	
	private MassaTestesReservarBilheteEnum(MassaTestesBilheteEnum bilheteEnum) {
		this.bilheteEnum = bilheteEnum;
	}
	
	public MassaTestesBilheteEnum getBilheteEnum() {
		return this.bilheteEnum;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.RESERVADO;
	}
	
	public static Collection<MassaTestesReservarBilheteEnum[]> getParameters(){
		Collection<MassaTestesReservarBilheteEnum[]> c = new ArrayList<MassaTestesReservarBilheteEnum[]>();
		for(MassaTestesReservarBilheteEnum ciaAerea : values()){
			c.add(new MassaTestesReservarBilheteEnum[] { ciaAerea });
		}
		return c;
	}	
}
