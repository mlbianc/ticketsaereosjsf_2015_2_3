package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_1_BSB_CWB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_1_CWB_GRU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_1_CWB_GRU_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_2_CWB_GRU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_2_GRU_CWB_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_2_GRU_SDU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_3_CWB_GRU_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_3_GRU_SDU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_3_SDU_BSB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_4_BSB_SDU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_4_GRU_CWB_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_4_SDU_BSB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_5_BSB_CWB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_5_CWB_GRU_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_5_SDU_BSB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_6_BSB_CWB_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_6_CWB_GRU_AZUL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum.HOR_DIA_6_GRU_CWB_GOL;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum.PASSAGEIRO_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum.PASSAGEIRO_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum.PASSAGEIRO_03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;


public enum MassaTestesBilheteEnum {
	
	PASSAGEIRO_01_BILHETE_01(PASSAGEIRO_01, HOR_DIA_1_BSB_CWB_AZUL, "EC", "33A"),
	PASSAGEIRO_01_BILHETE_02(PASSAGEIRO_01, HOR_DIA_2_CWB_GRU_AZUL, "EC", "32A"),
	PASSAGEIRO_01_BILHETE_03(PASSAGEIRO_01, HOR_DIA_3_GRU_SDU_AZUL, "EC", "11A"),
	PASSAGEIRO_01_BILHETE_04(PASSAGEIRO_01, HOR_DIA_4_SDU_BSB_AZUL, "EC", "8B"),
	PASSAGEIRO_01_BILHETE_05(PASSAGEIRO_01, HOR_DIA_5_BSB_CWB_AZUL, "EC", "7F"),
	PASSAGEIRO_01_BILHETE_06(PASSAGEIRO_01, HOR_DIA_6_CWB_GRU_AZUL, "EC", "15A"),
	
	PASSAGEIRO_02_BILHETE_01(PASSAGEIRO_02, HOR_DIA_1_CWB_GRU_AZUL, "EX", "34B"),
	PASSAGEIRO_02_BILHETE_02(PASSAGEIRO_02, HOR_DIA_2_GRU_SDU_AZUL, "EX", "21C"),
	PASSAGEIRO_02_BILHETE_03(PASSAGEIRO_02, HOR_DIA_3_SDU_BSB_AZUL, "EX", "5D"),
	PASSAGEIRO_02_BILHETE_04(PASSAGEIRO_02, HOR_DIA_4_BSB_SDU_AZUL, "EX", "6E"),
	PASSAGEIRO_02_BILHETE_05(PASSAGEIRO_02, HOR_DIA_5_SDU_BSB_AZUL, "EX", "22F"),
	PASSAGEIRO_02_BILHETE_06(PASSAGEIRO_02, HOR_DIA_6_BSB_CWB_AZUL, "EX", "19A"),

	PASSAGEIRO_03_BILHETE_01(PASSAGEIRO_03, HOR_DIA_1_CWB_GRU_GOL, "PR", "25A"),
	PASSAGEIRO_03_BILHETE_02(PASSAGEIRO_03, HOR_DIA_2_GRU_CWB_GOL, "PR", "14B"),
	PASSAGEIRO_03_BILHETE_03(PASSAGEIRO_03, HOR_DIA_3_CWB_GRU_GOL, "PR", "13C"),
	PASSAGEIRO_03_BILHETE_04(PASSAGEIRO_03, HOR_DIA_4_GRU_CWB_GOL, "PR", "8D"),
	PASSAGEIRO_03_BILHETE_05(PASSAGEIRO_03, HOR_DIA_5_CWB_GRU_GOL, "PR", "1E"),
	PASSAGEIRO_03_BILHETE_06(PASSAGEIRO_03, HOR_DIA_6_GRU_CWB_GOL, "PR", "4F");
	
	private MassaTestesPassageiroEnum passageiroEnum;
	private MassaTestesHorarioEnum horarioEnum;
	private TipoBilheteEnum tipoBilheteEnum;
	private String assento;
	
	private MassaTestesBilheteEnum(MassaTestesPassageiroEnum passageiroEnum, MassaTestesHorarioEnum horarioEnum, String tipoBilheteEnum, String assento) {
		this.passageiroEnum = passageiroEnum;
		this.horarioEnum = horarioEnum;
		this.assento = assento;
		
		//Explicacao sobre o metodo getBy no seguinte link - http://www.mauda.com.br/?p=354
		this.tipoBilheteEnum = TipoBilheteEnum.getByAcronimo(tipoBilheteEnum);
	}
	
	public MassaTestesPassageiroEnum getPassageiroEnum() {
		return this.passageiroEnum;
	}
	
	public MassaTestesHorarioEnum getHorarioEnum() {
		return this.horarioEnum;
	}
	
	public TipoBilheteEnum getTipoBilheteEnum() {
		return this.tipoBilheteEnum;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.DISPONIVEL;
	}
	
	public String getAssento() {
		return this.assento;
	}
	
	public static MassaTestesBilheteEnum getInstance(){
		Random generator = new Random();
		MassaTestesBilheteEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesBilheteEnum[]> getParameters(){
		Collection<MassaTestesBilheteEnum[]> c = new ArrayList<MassaTestesBilheteEnum[]>();
		for(MassaTestesBilheteEnum ciaAerea : values()){
			c.add(new MassaTestesBilheteEnum[] { ciaAerea });
		}
		return c;
	}	
}
