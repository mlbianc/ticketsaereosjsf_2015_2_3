package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_03;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_01;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_02;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_03;

import java.util.ArrayList;
import java.util.Collection;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;


public enum MassaTestesComprarBilheteEnum {
	
	COMPRAR_PASSAGEIRO_01_BILHETE_01(PASSAGEIRO_01_BILHETE_01),
	COMPRAR_PASSAGEIRO_01_BILHETE_02(PASSAGEIRO_01_BILHETE_02),
	COMPRAR_PASSAGEIRO_01_BILHETE_03(PASSAGEIRO_01_BILHETE_03),
	
	COMPRAR_PASSAGEIRO_02_BILHETE_01(PASSAGEIRO_02_BILHETE_01),
	COMPRAR_PASSAGEIRO_02_BILHETE_02(PASSAGEIRO_02_BILHETE_02),
	COMPRAR_PASSAGEIRO_02_BILHETE_03(PASSAGEIRO_02_BILHETE_03),

	COMPRAR_PASSAGEIRO_03_BILHETE_01(PASSAGEIRO_03_BILHETE_01),
	COMPRAR_PASSAGEIRO_03_BILHETE_02(PASSAGEIRO_03_BILHETE_02),
	COMPRAR_PASSAGEIRO_03_BILHETE_03(PASSAGEIRO_03_BILHETE_03);
	
	private MassaTestesBilheteEnum bilheteEnum;
	
	private MassaTestesComprarBilheteEnum(MassaTestesBilheteEnum bilheteEnum) {
		this.bilheteEnum = bilheteEnum;
	}
	
	public MassaTestesBilheteEnum getBilheteEnum() {
		return this.bilheteEnum;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.COMPRADO;
	}
	
	public static Collection<MassaTestesComprarBilheteEnum[]> getParameters(){
		Collection<MassaTestesComprarBilheteEnum[]> c = new ArrayList<MassaTestesComprarBilheteEnum[]>();
		for(MassaTestesComprarBilheteEnum ciaAerea : values()){
			c.add(new MassaTestesComprarBilheteEnum[] { ciaAerea });
		}
		return c;
	}	
}
