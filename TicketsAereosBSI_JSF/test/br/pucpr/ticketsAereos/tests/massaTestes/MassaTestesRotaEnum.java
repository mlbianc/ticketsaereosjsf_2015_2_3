package br.pucpr.ticketsAereos.tests.massaTestes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;



public enum MassaTestesRotaEnum {
	
	ROTA_BSB_CWB_AZUL("BSB-CWB-Azul", "Brasilia para Curitiba via Azul", MassaTestesAeroportoEnum.AERO_BRASILIA, MassaTestesAeroportoEnum.AERO_CURITIBA, MassaTestesCiaAereaEnum.AZUL),
	ROTA_BSB_SDU_AZUL("BSB-SDU-Azul", "Brasilia para Rio de Janeiro via Azul", MassaTestesAeroportoEnum.AERO_BRASILIA, MassaTestesAeroportoEnum.AERO_SANTOS_DUMONT, MassaTestesCiaAereaEnum.AZUL),
	
	ROTA_CWB_GRU_AZUL("CWB-GRU-Azul", "Curitiba para Guarulhos via Azul", MassaTestesAeroportoEnum.AERO_CURITIBA, MassaTestesAeroportoEnum.AERO_GUARULHOS, MassaTestesCiaAereaEnum.AZUL),
	
	ROTA_GRU_SDU_AZUL("GRU-SDU-Azul", "Guarulhos para Rio de Janeiro via Azul", MassaTestesAeroportoEnum.AERO_GUARULHOS, MassaTestesAeroportoEnum.AERO_SANTOS_DUMONT, MassaTestesCiaAereaEnum.AZUL),
	
	ROTA_SDU_BSB_AZUL("SDU-BSB-Azul", "Rio de Janeiro para Brasilia via Azul", MassaTestesAeroportoEnum.AERO_SANTOS_DUMONT, MassaTestesAeroportoEnum.AERO_BRASILIA, MassaTestesCiaAereaEnum.AZUL),	
	
	
	ROTA_CWB_GRU_GOL("CWB-GRU-Gol", "Curitiba para Guarulhos via Gol", MassaTestesAeroportoEnum.AERO_CURITIBA, MassaTestesAeroportoEnum.AERO_GUARULHOS, MassaTestesCiaAereaEnum.GOL),
	ROTA_GRU_CWB_GOL("GRU-CWB-Gol", "Guarulhos para Curitiba via Gol", MassaTestesAeroportoEnum.AERO_GUARULHOS, MassaTestesAeroportoEnum.AERO_CURITIBA, MassaTestesCiaAereaEnum.GOL);
	
	private String nome, descricao;
	private MassaTestesAeroportoEnum origem, destino;
	private MassaTestesCiaAereaEnum ciaAerea;
	
	private MassaTestesRotaEnum(String nome, String descricao, MassaTestesAeroportoEnum origem, MassaTestesAeroportoEnum destino, MassaTestesCiaAereaEnum ciaAereaEnum) {
		this.nome = nome;
		this.descricao = descricao;
		this.origem = origem;
		this.destino = destino;
		this.ciaAerea = ciaAereaEnum;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public MassaTestesAeroportoEnum getOrigem() {
		return this.origem;
	}
	
	public MassaTestesAeroportoEnum getDestino() {
		return this.destino;
	}
	
	public MassaTestesCiaAereaEnum getCiaAerea() {
		return this.ciaAerea;
	}
	
	public static MassaTestesRotaEnum getInstance(){
		Random generator = new Random();
		MassaTestesRotaEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesRotaEnum[]> getParameters(){
		Collection<MassaTestesRotaEnum[]> c = new ArrayList<MassaTestesRotaEnum[]>();
		for(MassaTestesRotaEnum ciaAerea : values()){
			c.add(new MassaTestesRotaEnum[] { ciaAerea });
		}
		return c;
	}	
}
