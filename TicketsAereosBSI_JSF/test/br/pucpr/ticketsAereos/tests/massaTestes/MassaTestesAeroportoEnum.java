package br.pucpr.ticketsAereos.tests.massaTestes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;



public enum MassaTestesAeroportoEnum {
	
	AERO_BRASILIA("Brasilia", "BSB", MassaTestesEnderecoEnum.END_BRASILIA),
	AERO_CURITIBA("Curitiba", "CWB", MassaTestesEnderecoEnum.END_CURITIBA),
	AERO_GUARULHOS("Guarulhos", "GRU", MassaTestesEnderecoEnum.END_SAO_PAULO),
	AERO_SANTOS_DUMONT("Santos Dumont", "SDU", MassaTestesEnderecoEnum.END_RIO_JANEIRO);
	
	private String nome, codigo;
	private MassaTestesEnderecoEnum enderecoEnum;
	
	private MassaTestesAeroportoEnum(String nome, String codigo, MassaTestesEnderecoEnum enderecoEnum) {
		this.nome = nome;
		this.codigo = codigo;
		this.enderecoEnum = enderecoEnum;
	}
	
	public String getCodigo() {
		return this.codigo;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public MassaTestesEnderecoEnum getEnderecoEnum() {
		return this.enderecoEnum;
	}
	
	public static MassaTestesAeroportoEnum getInstance(){
		Random generator = new Random();
		MassaTestesAeroportoEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}	
	
	public static Collection<MassaTestesAeroportoEnum[]> getParameters(){
		Collection<MassaTestesAeroportoEnum[]> c = new ArrayList<MassaTestesAeroportoEnum[]>();
		for(MassaTestesAeroportoEnum ciaAerea : values()){
			c.add(new MassaTestesAeroportoEnum[] { ciaAerea });
		}
		return c;
	}
	
	@Override
	public String toString() {
		return this.name();
	}
}
