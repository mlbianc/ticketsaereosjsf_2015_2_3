package br.pucpr.ticketsAereos.tests.massaTestes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public enum MassaTestesEnderecoEnum {
	 
	END_BRASILIA("Brasilia", "BSB", "DF047", 1, "Aeroporto", "Lago Sul", "Brasilia", "DF", "Brasil"),
	END_BELO_HORIZONTE("Confins", "CNF", "MG010", 1, "Kilometro 39", "Confins", "Belo Horizonte", "MG", "Brasil"),
	END_CURITIBA("Curitiba", "CWB", "Av. Rocha Pombo", 1, "Aeroporto", "Aguas Belas", "Sao Jose dos Pinhais", "PR", "Brasil"),
	END_SAO_PAULO("Guarulhos", "GRU", "Rod. Helio Smidt", 1, "Aeroporto", "Cumbica", "Guarulhos", "SP", "Brasil"),
	END_PORTO_ALEGRE("Porto Alegre", "POA", "Av. Severo Dulius", 1, "Aeroporto", "Centro", "Porto Alegre", "RS", "Brasil"),
	END_RIO_JANEIRO("Santos Dumont", "SDU", "Praca Sen. Salgado Filho", 1, "Aeroporto", "Centro", "Rio de Janeiro", "RJ", "Brasil");
	
	private String nome, codigo, rua, complemento, bairro, cidade, estado, pais;
	private int numero;
	
	private MassaTestesEnderecoEnum(String nome, String codigo, String rua, int numero, 
			String complemento, String bairro, String cidade, String estado, String pais) {
		this.nome = nome;
		this.codigo = codigo;
		this.rua = rua;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.pais = pais;
	}
	
	public String getBairro() {
		return bairro;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public String getNome() {
		return nome;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public String getPais() {
		return pais;
	}
	
	public String getRua() {
		return rua;
	}
	
	public static MassaTestesEnderecoEnum getInstance(){
		Random generator = new Random();
		MassaTestesEnderecoEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}	
	
	public static Collection<MassaTestesEnderecoEnum[]> getParameters(){
		Collection<MassaTestesEnderecoEnum[]> c = new ArrayList<MassaTestesEnderecoEnum[]>();
		for(MassaTestesEnderecoEnum ciaAerea : values()){
			c.add(new MassaTestesEnderecoEnum[] { ciaAerea });
		}
		return c;
	}	
	
	@Override
	public String toString() {
		return this.name();
	}
}
