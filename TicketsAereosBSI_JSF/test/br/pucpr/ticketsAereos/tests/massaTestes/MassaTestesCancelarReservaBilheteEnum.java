package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_01_BILHETE_04;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_02_BILHETE_04;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum.PASSAGEIRO_03_BILHETE_04;

import java.util.ArrayList;
import java.util.Collection;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;


public enum MassaTestesCancelarReservaBilheteEnum {
	
	CANCELAR_RESERVA_PASSAGEIRO_01_BILHETE_04(PASSAGEIRO_01_BILHETE_04),
	
	CANCELAR_RESERVA_PASSAGEIRO_02_BILHETE_04(PASSAGEIRO_02_BILHETE_04),
	
	CANCELAR_RESERVA_PASSAGEIRO_03_BILHETE_04(PASSAGEIRO_03_BILHETE_04);
	
	private MassaTestesBilheteEnum bilheteEnum;
	
	private MassaTestesCancelarReservaBilheteEnum(MassaTestesBilheteEnum bilheteEnum) {
		this.bilheteEnum = bilheteEnum;
	}
	
	public MassaTestesBilheteEnum getBilheteEnum() {
		return this.bilheteEnum;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.DISPONIVEL;
	}
	
	public static Collection<MassaTestesCancelarReservaBilheteEnum[]> getParameters(){
		Collection<MassaTestesCancelarReservaBilheteEnum[]> c = new ArrayList<MassaTestesCancelarReservaBilheteEnum[]>();
		for(MassaTestesCancelarReservaBilheteEnum ciaAerea : values()){
			c.add(new MassaTestesCancelarReservaBilheteEnum[] { ciaAerea });
		}
		return c;
	}	
}
