package br.pucpr.ticketsAereos.tests.massaTestes;

import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_PORTO_ALEGRE;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_RIO_JANEIRO;
import static br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum.END_SAO_PAULO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import br.pucpr.ticketsAereos.utils.DateUtils;

public enum MassaTestesPassageiroEnum{
	
	PASSAGEIRO_01('P', "Passageiro 01", "passageiro01@passageiros.com.br", "11111111", "09/03/1997", 
			"3456345634563456", "12121212", END_PORTO_ALEGRE),
			
	PASSAGEIRO_02('P', "Passageiro 02", "passageiro02@passageiros.com.br", "22222222", "07/05/2000", 
			"1234567812345678", "56565656", END_RIO_JANEIRO),
			
	PASSAGEIRO_03('P', "Passageiro 03", "passageiro03@passageiros.com.br", "33333333", "20/07/1954", 
			"7890789078907890", "34343434", END_SAO_PAULO);
	
	
	private char tipo;
	private String nome, email, telefone, numeroCartao, documento;
	private Date dataNascimento;
	private MassaTestesEnderecoEnum enderecoEnum;
	
	private MassaTestesPassageiroEnum(char tipo, String nome, String email, String telefone, String dataNascimento,  
				String cartao, String documento, MassaTestesEnderecoEnum enderecoEnum) {
		this.tipo = tipo;
		this.dataNascimento = DateUtils.converterDate(dataNascimento);
		this.email = email;
		this.nome = nome;
		this.telefone = telefone;
		this.enderecoEnum = enderecoEnum;
		
		this.numeroCartao = cartao;
		this.documento = documento;
	}
	
	public char getTipo() {
		return this.tipo;
	}
	
	public Date getDataNascimento() {
		return this.dataNascimento;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getTelefone() {
		return this.telefone;
	}
	
	public String getNumeroCartao() {
		return this.numeroCartao;
	}
	
	public String getDocumento() {
		return this.documento;
	}
	
	public MassaTestesEnderecoEnum getEnderecoEnum() {
		return this.enderecoEnum;
	}
	
	public static MassaTestesPassageiroEnum getInstance(){
		Random generator = new Random();
		MassaTestesPassageiroEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static Collection<MassaTestesPassageiroEnum[]> getParameters(){
		Collection<MassaTestesPassageiroEnum[]> c = new ArrayList<MassaTestesPassageiroEnum[]>();
		for(MassaTestesPassageiroEnum ciaAerea : values()){
			c.add(new MassaTestesPassageiroEnum[] { ciaAerea });
		}
		return c;
	}		
}
