package br.pucpr.ticketsAereos.tests.massaTestes;

import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBagagemEnum;


public enum MassaTestesBagagemEnum {
	
	BAGAGEM_MAO_01('M', 4.3),
	BAGAGEM_MAO_02('M', 4.5),
	BAGAGEM_MAO_03('M', 3.2),
	BAGAGEM_MAO_04('M', 3.1),
	BAGAGEM_MAO_05('M', 2.2),
	BAGAGEM_MAO_06('M', 3.6),
	
	BAGAGEM_NACIONAL_01('N', 20.0),
	BAGAGEM_NACIONAL_02('N', 18.2),
	BAGAGEM_NACIONAL_03('N', 15.4),
	BAGAGEM_NACIONAL_04('N', 13.5),
	BAGAGEM_NACIONAL_05('N', 21.6),
	BAGAGEM_NACIONAL_06('N', 22.8),
	
	BAGAGEM_INTERNACIONAL_01('I', 12.3),
	BAGAGEM_INTERNACIONAL_02('I', 16.8),
	BAGAGEM_INTERNACIONAL_03('I', 13.4),
	BAGAGEM_INTERNACIONAL_04('I', 20.8),
	BAGAGEM_INTERNACIONAL_05('I', 25.9),
	BAGAGEM_INTERNACIONAL_06('I', 30.5);

	private TipoBagagemEnum tipoBagagemEnum;
	private Double peso;
	
	private MassaTestesBagagemEnum(char tipoBagagemEnum, double peso) {
		this.tipoBagagemEnum = TipoBagagemEnum.getBy(tipoBagagemEnum);
		this.peso = peso;
	}
	
	public SituacaoBilheteEnum getSituacaoEnum() {
		return SituacaoBilheteEnum.CHECKIN;
	}
	
	public TipoBagagemEnum getTipoBagagemEnum() {
		return tipoBagagemEnum;
	}
	
	public Double getPeso() {
		return peso;
	}
}
