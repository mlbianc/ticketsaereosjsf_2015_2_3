package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.AeroportoBC;
import br.pucpr.ticketsAereos.dto.AeroportoDTO;
import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;
import br.pucpr.ticketsAereos.tests.verificador.AeroportoVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TesteAeroportoDAOQueries extends AbstractCrudTestDAOQueries<Aeroporto, AeroportoDTO, AeroportoBC, MassaTestesAeroportoEnum>{
	
	///////////////////////////////////////////////////////////
	// CONSTRUTOR
	///////////////////////////////////////////////////////////
	public TesteAeroportoDAOQueries() {
		verificator = AeroportoVerificator.getInstance();
		bc = AeroportoBC.getInstance();
	}

	///////////////////////////////////////////////////////////
	// METODOS ABSTRATOS - IMPLEMENTACAO
	///////////////////////////////////////////////////////////
	@Override
	protected int getEnumSize() {
		return MassaTestesAeroportoEnum.values().length;
	}	
	
	///////////////////////////////////////////////////////////
	// METODOS AUXILIARES
	///////////////////////////////////////////////////////////
	@Before
	public void init() {
		this.filter = new AeroportoDTO();
	}	

	///////////////////////////////////////////////////////////
	// DADOS DA PESSOA
	///////////////////////////////////////////////////////////

	@Test
	public void testFindByFilterCodigoAeroporto(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCodigo(aeroportoEnum.getCodigo());
			
			//Obtem as informacoes do banco de dados
			List<Aeroporto> aeroportoFilter = (List<Aeroporto>) bc.findByFilter(filter);
			
			Assert.assertEquals(aeroportoFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(aeroportoFilter.get(0), aeroportoEnum);
		}
	}
	
	@Test
	public void testFindByFilterNomeAeroporto(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setNome(aeroportoEnum.getNome());
			
			//Obtem as informacoes do banco de dados
			List<Aeroporto> aeroportoFilter = (List<Aeroporto>) bc.findByFilter(filter);
			
			Assert.assertEquals(aeroportoFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(aeroportoFilter.get(0), aeroportoEnum);
		}
	}	
	
	///////////////////////////////////////////////////////////
	// DADOS DO ENDERECO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterCidadeEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCidade(enderecoEnum.getCidade());

			//Obtem as informacoes do banco de dados
			Collection<Aeroporto> aeroportoFilter = bc.findByFilter(filter);
			
			for (Aeroporto aeroporto : aeroportoFilter) {
				Assert.assertEquals(aeroporto.getEndereco().getCidade(), enderecoEnum.getCidade());				
			}
		}
	}
	
	@Test
	public void testFindByFilterEstadoEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setEstado(enderecoEnum.getEstado());

			//Obtem as informacoes do banco de dados
			Collection<Aeroporto> aeroportoFilter = bc.findByFilter(filter);
			
			for (Aeroporto aeroporto : aeroportoFilter) {
				Assert.assertEquals(aeroporto.getEndereco().getEstado(), enderecoEnum.getEstado());				
			}
		}
	}
	
	@Test
	public void testFindByFilterPaisEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setPais(enderecoEnum.getPais());

			//Obtem as informacoes do banco de dados
			Collection<Aeroporto> aeroportoFilter = bc.findByFilter(filter);
			
			for (Aeroporto aeroporto : aeroportoFilter) {
				Assert.assertEquals(aeroporto.getEndereco().getPais(), enderecoEnum.getPais());				
			}
		}
	}
}
