package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.PassageiroBC;
import br.pucpr.ticketsAereos.dto.PassageiroDTO;
import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.tests.verificador.EnderecoVerificator;
import br.pucpr.ticketsAereos.tests.verificador.PassageiroVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TestePassageiroDAOQueries extends AbstractCrudTestDAOQueries<Passageiro, PassageiroDTO, PassageiroBC, MassaTestesPassageiroEnum>{
	
	///////////////////////////////////////////////////////////
	// CONSTRUTOR
	///////////////////////////////////////////////////////////
	public TestePassageiroDAOQueries() {
		verificator = PassageiroVerificator.getInstance();
		bc = PassageiroBC.getInstance();
	}

	///////////////////////////////////////////////////////////
	// METODOS ABSTRATOS - IMPLEMENTACAO
	///////////////////////////////////////////////////////////
	@Override
	protected int getEnumSize() {
		return MassaTestesPassageiroEnum.values().length;
	}	
	
	///////////////////////////////////////////////////////////
	// METODOS AUXILIARES
	///////////////////////////////////////////////////////////
	@Before
	public void init() {
		this.filter = new PassageiroDTO();
	}	

	///////////////////////////////////////////////////////////
	// DADOS DA PESSOA
	///////////////////////////////////////////////////////////

	@Test
	public void testFindByFilterDataNascimentoPassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setDataNascimento(pessoaEnum.getDataNascimento());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(passageiroFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(passageiroFilter.get(0), pessoaEnum);
		}
	}
	
	@Test
	public void testFindByFilterEmailPassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setEmail(pessoaEnum.getEmail());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(passageiroFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(passageiroFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterNomePassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setNome(pessoaEnum.getNome());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(passageiroFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(passageiroFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterTelefonePassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setTelefone(pessoaEnum.getTelefone());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(passageiroFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(passageiroFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterDocumentoPassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setDocumento(pessoaEnum.getDocumento());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> pessoaFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}
	
	@Test
	public void testFindByFilterNumeroCartaoPassageiro(){
		for (MassaTestesPassageiroEnum pessoaEnum : MassaTestesPassageiroEnum.values()) {
			//Seta a informacao do filtro
			filter.setNumeroCartao(pessoaEnum.getNumeroCartao());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> pessoaFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}
	
	///////////////////////////////////////////////////////////
	// DADOS DO ENDERECO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterCidadeEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCidade(enderecoEnum.getCidade());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			for (Passageiro pessoa : passageiroFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco(), enderecoEnum);
			}
		}
	}
	
	@Test
	public void testFindByFilterEstadoEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setEstado(enderecoEnum.getEstado());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			for (Passageiro pessoa : passageiroFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco(), enderecoEnum);
			}
		}
	}
	
	@Test
	public void testFindByFilterPaisEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setPais(enderecoEnum.getPais());
			
			//Obtem as informacoes do banco de dados
			List<Passageiro> passageiroFilter = (List<Passageiro>) bc.findByFilter(filter);
			
			for (Passageiro pessoa : passageiroFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco());
				Assert.assertEquals(pessoa.getEndereco().getPais(), enderecoEnum.getPais());
			}
		}
	}
}
