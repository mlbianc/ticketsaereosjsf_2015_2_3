package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.RotaBC;
import br.pucpr.ticketsAereos.dto.RotaDTO;
import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;
import br.pucpr.ticketsAereos.tests.verificador.AeroportoVerificator;
import br.pucpr.ticketsAereos.tests.verificador.CiaAereaVerificator;
import br.pucpr.ticketsAereos.tests.verificador.RotaVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteRotaDAOQueries extends AbstractCrudTestDAOQueries<Rota, RotaDTO, RotaBC, MassaTestesRotaEnum> {
	
	///////////////////////////////////////////////////////////
	// Construtor
	///////////////////////////////////////////////////////////
	public TesteRotaDAOQueries() {
		verificator = RotaVerificator.getInstance();
		bc = RotaBC.getInstance();
	}
	
	///////////////////////////////////////////////////////////
	// Metodos Abstratos - Implementacao
	///////////////////////////////////////////////////////////
	@Override
	protected int getEnumSize() {
		return MassaTestesRotaEnum.values().length;
	}	
	
	// ////////////////////////////////////////
	// METODOS AUXILIARES
	// ////////////////////////////////////////
	@Before
	public void init() {
		this.filter = new RotaDTO();
	}	
	
	///////////////////////////////////////////////////////////
	// DADOS DA ROTA
	///////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterNomeRota(){
		for (MassaTestesRotaEnum rotaEnum : MassaTestesRotaEnum.values()) {
			//Seta a informacao do filtro
			filter.setNome(rotaEnum.getNome());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			Assert.assertEquals(rotaFilter.size(), 1);
			
			//Verifica se sao iguais
			verificator.verify(rotaFilter.get(0), rotaEnum);
		}
	}
	
	///////////////////////////////////////////////////////////
	// DADOS DA CIAAEREA
	///////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterCiaAerea(){
		for (MassaTestesCiaAereaEnum ciaAereaEnum : MassaTestesCiaAereaEnum.values()) {
			//Seta a informacao do filtro
			filter.setNomeCiaAerea(ciaAereaEnum.getNome());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			for (Rota rota : rotaFilter) {
				CiaAereaVerificator.getInstance().verify(rota.getCiaAerea(), ciaAereaEnum);
			}
		}
	}	
	
	///////////////////////////////////////////////////////////
	// DADOS DO AEROPORTO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterNomeOrigem(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setAeroportoOrigem(aeroportoEnum.getNome());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			for (Rota rota : rotaFilter) {
				AeroportoVerificator.getInstance().verify(rota.getOrigem(), aeroportoEnum);
			}
		}
	}	
	
	@Test
	public void testFindByFilterCidadeOrigem(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCidadeOrigem(aeroportoEnum.getEnderecoEnum().getCidade());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			for (Rota rota : rotaFilter) {
				AeroportoVerificator.getInstance().verify(rota.getOrigem(), aeroportoEnum);
			}
		}
	}	
	
	@Test
	public void testFindByFilterNomeDestino(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setAeroportoDestino(aeroportoEnum.getNome());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			for (Rota rota : rotaFilter) {
				AeroportoVerificator.getInstance().verify(rota.getDestino(), aeroportoEnum);
			}
		}
	}	
	
	@Test
	public void testFindByFilterCidadeDestino(){
		for (MassaTestesAeroportoEnum aeroportoEnum : MassaTestesAeroportoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCidadeDestino(aeroportoEnum.getEnderecoEnum().getCidade());

			//Obtem as informacoes do banco de dados
			List<Rota> rotaFilter = (List<Rota>) bc.findByFilter(filter);
			
			for (Rota rota : rotaFilter) {
				AeroportoVerificator.getInstance().verify(rota.getDestino(), aeroportoEnum);
			}
		}
	}	
}
