package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.FuncionarioBC;
import br.pucpr.ticketsAereos.dto.FuncionarioDTO;
import br.pucpr.ticketsAereos.model.Funcionario;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.tests.verificador.CiaAereaVerificator;
import br.pucpr.ticketsAereos.tests.verificador.EnderecoVerificator;
import br.pucpr.ticketsAereos.tests.verificador.FuncionarioVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TesteFuncionarioDAOQueries extends AbstractCrudTestDAOQueries<Funcionario, FuncionarioDTO, FuncionarioBC, MassaTestesFuncionarioEnum>{
	
	///////////////////////////////////////////////////////////
	// CONSTRUTOR
	///////////////////////////////////////////////////////////
	public TesteFuncionarioDAOQueries() {
		verificator = FuncionarioVerificator.getInstance();
		bc = FuncionarioBC.getInstance();
	}

	///////////////////////////////////////////////////////////
	// METODOS ABSTRATOS - IMPLEMENTACAO
	///////////////////////////////////////////////////////////
	@Override
	protected int getEnumSize() {
		return MassaTestesFuncionarioEnum.values().length;
	}	
	
	///////////////////////////////////////////////////////////
	// METODOS AUXILIARES
	///////////////////////////////////////////////////////////
	@Before
	public void init() {
		this.filter = new FuncionarioDTO();
	}	

	///////////////////////////////////////////////////////////
	// DADOS DO FUNCIONARIO
	///////////////////////////////////////////////////////////

	@Test
	public void testFindByFilterDataNascimentoFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setDataNascimento(pessoaEnum.getDataNascimento());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}
	
	@Test
	public void testFindByFilterEmailFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setEmail(pessoaEnum.getEmail());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterNomeFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setNome(pessoaEnum.getNome());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterTelefoneFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setTelefone(pessoaEnum.getTelefone());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}	
	
	@Test
	public void testFindByFilterCodigoFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setCodigo(pessoaEnum.getCodigo());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}
	
	@Test
	public void testFindByFilterContaCorrenteFuncionario(){
		for (MassaTestesFuncionarioEnum pessoaEnum : MassaTestesFuncionarioEnum.values()) {
			//Seta a informacao do filtro
			filter.setContaCorrente(pessoaEnum.getContaCorrente());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(pessoaFilter.get(0), pessoaEnum);
		}
	}	
	
	
	///////////////////////////////////////////////////////////
	// DADOS DA CIAAEREA
	///////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterCiaAerea() {
		for (MassaTestesCiaAereaEnum ciaAereaEnum : MassaTestesCiaAereaEnum.values()) {
			// Seta a informacao do filtro
			filter.setNomeCiaAerea(ciaAereaEnum.getNome());

			// Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);

			for (Funcionario pessoa : pessoaFilter) {
				CiaAereaVerificator.getInstance().verify(pessoa.getCiaAerea(),
						ciaAereaEnum);
			}
		}
	}	
		
	
	///////////////////////////////////////////////////////////
	// DADOS DO ENDERECO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterCidadeEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setCidade(enderecoEnum.getCidade());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			for (Funcionario pessoa : pessoaFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco(), enderecoEnum);
			}
		}
	}
	
	@Test
	public void testFindByFilterEstadoEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setEstado(enderecoEnum.getEstado());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			for (Funcionario pessoa : pessoaFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco(), enderecoEnum);
			}
		}
	}
	
	@Test
	public void testFindByFilterPaisEndereco(){
		for (MassaTestesEnderecoEnum enderecoEnum : MassaTestesEnderecoEnum.values()) {
			//Seta a informacao do filtro
			filter.setPais(enderecoEnum.getPais());
			
			//Obtem as informacoes do banco de dados
			List<Funcionario> pessoaFilter = (List<Funcionario>) bc.findByFilter(filter);
			
			for (Funcionario pessoa : pessoaFilter) {
				//Verifica se os objetos enderecos sao iguais
				EnderecoVerificator.getInstance().verify(pessoa.getEndereco());
				Assert.assertEquals(pessoa.getEndereco().getPais(), enderecoEnum.getPais());
			}
		}
	}
}
