package br.pucpr.ticketsAereos.tests.bc.actions;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.BilheteBC;
import br.pucpr.ticketsAereos.bc.BilheteBCFactory;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesReservarBilheteEnum;
import br.pucpr.ticketsAereos.tests.model.actions.TesteReservarBilheteModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteReservarBilheteBC extends TesteReservarBilheteModel {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected TipoBilheteEnum tipoBilhete;
	protected BilheteBC<Bilhete, ?> bilheteBC;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	public TesteReservarBilheteBC(MassaTestesReservarBilheteEnum reservarBilheteEnum) {
		super(reservarBilheteEnum);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	@SuppressWarnings("unchecked")
	@Before
	@Override
	public void init() {
		super.init();
		
		//Obtem o tipo do bilhete
		this.tipoBilhete = this.reservarBilheteEnum.getBilheteEnum().getTipoBilheteEnum();
		
		//Obtem o BC correspondente
		this.bilheteBC = BilheteBCFactory.getInstanceBC(this.tipoBilhete);
	}
	
	
	/**
	 * Metodo que realiza a acao de Reservar na BC
	 */
	@Override
	protected void realizarAcaoReservar() {
		//Realiza a acao de reservar
		this.bilheteBC.reservar(this.bilhete, this.passageiro, this.assento);
	}
	
	/**
	 * Valida o bilhete nulo
	 */
	@Test(expected = BSIException.class)
	public void validarReservaSemBilhete(){
		//Atribui null ao bilhete
		this.bilhete = null;
		
		//Realiza a acao de reservar
		this.realizarAcaoReservar();
	}
	
	/**
	 * Valida o bilhete sem o passageiro
	 */
	@Test(expected = BSIException.class)
	public void validarReservaSemPassageiro(){
		//Atribui null a pessoa
		this.passageiro = null;
		
		//Realiza a acao de reservar
		this.realizarAcaoReservar();
	}	
	
	/**
	 * Valida o bilhete preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarReservaSemAssento(){
		//Atribui null ao assento
		this.assento = null;
		
		//Realiza a acao de reservar
		this.realizarAcaoReservar();
	}		
	
	/**
	 * Valida o bilhete preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarCompraComAssentoemBranco(){
		//Atribui branco ao assento
		this.assento = "                         ";
		
		//Realiza a acao de reservar
		this.realizarAcaoReservar();
	}
	
	/**
	 * Valida o bilhete em outra situacao que nao a disponivel
	 */
	@Test(expected = BSIException.class)
	public void validarCompraSemSituacaoDisponivel(){
		//Muda a situacao do bilhete
		this.bilhete.setSituacaoEnum(SituacaoBilheteEnum.COMPRADO);
		
		//Realiza a acao de reservar
		this.realizarAcaoReservar();
	}	
	
}
