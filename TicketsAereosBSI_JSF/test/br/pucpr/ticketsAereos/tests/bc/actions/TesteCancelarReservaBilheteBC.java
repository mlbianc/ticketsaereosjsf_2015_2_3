package br.pucpr.ticketsAereos.tests.bc.actions;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.BilheteBC;
import br.pucpr.ticketsAereos.bc.BilheteBCFactory;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCancelarReservaBilheteEnum;
import br.pucpr.ticketsAereos.tests.model.actions.TesteCancelarReservaBilheteModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteCancelarReservaBilheteBC extends TesteCancelarReservaBilheteModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected TipoBilheteEnum tipoBilhete;
	protected BilheteBC<Bilhete, ?> bilheteBC;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	public TesteCancelarReservaBilheteBC(MassaTestesCancelarReservaBilheteEnum cancelarBilheteEnum) {
		super(cancelarBilheteEnum);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Before
	@Override
	public void init() {
		//Obtem o tipo do bilhete
		this.tipoBilhete = this.cancelarBilheteEnum.getBilheteEnum().getTipoBilheteEnum();
		
		//Obtem o BC correspondente
		this.bilheteBC = BilheteBCFactory.getInstanceBC(this.tipoBilhete);
		
		super.init();
	}
	
	/**
	 * Metodo que realiza a acao de Reservar na BC
	 */
	@Override
	protected void realizarAcaoReservar() {
		//Realiza a acao de reservar
		this.bilheteBC.reservar(this.bilhete, this.passageiro, this.assento);
	}
	
	@Override
	protected void realizarAcaoCancelarReserva() {
		//Realiza a acao de Comprar
		this.bilheteBC.cancelarReserva(this.bilhete);
	}
	
	/**
	 * Valida o cancelamento da reserva com o bilhete nulo
	 */
	@Test(expected = BSIException.class)
	public void validarCheckInSemBilhete(){
		//Atribui null ao bilhete
		this.bilhete = null;
		
		//Realiza a acao de Check In
		this.realizarAcaoCancelarReserva();
	}
	
	/**
	 * Valida o bilhete em outra situacao que nao a reservado
	 */
	@Test(expected = BSIException.class)
	public void validarBilheteSemSituacaoDisponivel(){
		//Muda a situacao do bilhete
		this.bilhete.setSituacaoEnum(SituacaoBilheteEnum.DISPONIVEL);
		
		//Realiza a acao de Cancelar
		this.realizarAcaoCancelarReserva();
	}	
}
