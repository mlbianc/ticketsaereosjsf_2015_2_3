package br.pucpr.ticketsAereos.tests.bc.actions;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.BilheteBC;
import br.pucpr.ticketsAereos.bc.BilheteBCFactory;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.Bagagem;
import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCheckInBilheteEnum;
import br.pucpr.ticketsAereos.tests.model.actions.TesteCheckInBilheteModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteCheckInBilheteBC extends TesteCheckInBilheteModel {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected TipoBilheteEnum tipoBilhete;
	protected BilheteBC<Bilhete, ?> bilheteBC;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	public TesteCheckInBilheteBC(MassaTestesCheckInBilheteEnum checkInBilheteEnum) {
		super(checkInBilheteEnum);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Before
	@Override
	public void init() {
		//Obtem o tipo do bilhete
		this.tipoBilhete = this.checkInBilheteEnum.getBilheteEnum().getTipoBilheteEnum();
		
		//Obtem o BC correspondente
		this.bilheteBC = BilheteBCFactory.getInstanceBC(this.tipoBilhete);
		
		super.init();
	}
	
	/**
	 * Metodo que realiza a acao de Reservar na BC
	 */
	@Override
	protected void realizarAcaoReservar() {
		//Realiza a acao de reservar
		this.bilheteBC.reservar(this.bilhete, this.passageiro, this.assento);
	}
	
	
	/**
	 * Metodo que realiza a acao de Comprar na BC
	 */
	@Override
	protected void realizarAcaoComprar() {
		//Realiza a acao de Comprar
		this.bilheteBC.comprar(this.bilhete);
	}
	
	/**
	 * Metodo que realiza a acao de CheckIn na BC
	 */
	@Override
	protected Bagagem realizarAcaoCheckIn(){
		//Realiza a acao de checkIn
		return this.bilheteBC.realizarCheckIn(this.bilhete, this.tipoBagagem, this.peso);
	}	
	
	/**
	 * Valida o check in com o bilhete nulo
	 */
	@Test(expected = BSIException.class)
	public void validarCheckInSemBilhete(){
		//Atribui null ao bilhete
		this.bilhete = null;
		
		//Realiza a acao de Check In
		this.realizarAcaoCheckIn();
	}
	
	/**
	 * Valida o check in com o tipo bagagem nula
	 */
	@Test(expected = BSIException.class)
	public void validarCheckInSemTipoBagagem(){
		//Atribui null ao bilhete
		this.tipoBagagem = null;
		
		//Realiza a acao de Check In
		this.realizarAcaoCheckIn();
	}
	
	/**
	 * Valida o check in com o peso nulo
	 */
	@Test(expected = BSIException.class)
	public void validarCheckInSemPeso(){
		//Atribui null ao bilhete
		this.peso = null;
		
		//Realiza a acao de Check In
		this.realizarAcaoCheckIn();
	}
	
	/**
	 * Valida o check in com o peso negativo
	 */
	@Test(expected = BSIException.class)
	public void validarCheckInSemPesoNegativo(){
		//Atribui null ao bilhete
		this.peso = -100D;
		
		//Realiza a acao de Check In
		this.realizarAcaoCheckIn();
	}
	
	/**
	 * Valida o bilhete em outra situacao que nao a comprado
	 */
	@Test(expected = BSIException.class)
	public void validarBilheteSemSituacaoComprado(){
		//Muda a situacao do bilhete
		this.bilhete.setSituacaoEnum(SituacaoBilheteEnum.DISPONIVEL);
		
		//Realiza a acao de Check In
		this.realizarAcaoCheckIn();
	}	
}
