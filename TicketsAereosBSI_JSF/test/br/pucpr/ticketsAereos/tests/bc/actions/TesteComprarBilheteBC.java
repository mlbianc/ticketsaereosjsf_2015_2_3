package br.pucpr.ticketsAereos.tests.bc.actions;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.BilheteBC;
import br.pucpr.ticketsAereos.bc.BilheteBCFactory;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesComprarBilheteEnum;
import br.pucpr.ticketsAereos.tests.model.actions.TesteComprarBilheteModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteComprarBilheteBC extends TesteComprarBilheteModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected TipoBilheteEnum tipoBilhete;
	protected BilheteBC<Bilhete, ?> bilheteBC;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	public TesteComprarBilheteBC(MassaTestesComprarBilheteEnum comprarBilheteEnum) {
		super(comprarBilheteEnum);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	@SuppressWarnings("unchecked")
	@Before
	@Override
	public void init() {
		//Obtem o tipo do bilhete
		this.tipoBilhete = this.comprarBilheteEnum.getBilheteEnum().getTipoBilheteEnum();
		
		//Obtem o BC correspondente
		this.bilheteBC = BilheteBCFactory.getInstanceBC(this.tipoBilhete);
		
		super.init();
	}
	
	/**
	 * Metodo que realiza a acao de Reservar na BC
	 */
	@Override
	protected void realizarAcaoReservar() {
		//Realiza a acao de reservar
		this.bilheteBC.reservar(this.bilhete, this.passageiro, this.assento);
	}
	
	@Override
	protected void realizarAcaoComprar() {
		//Realiza a acao de Comprar
		this.bilheteBC.comprar(this.bilhete);
	}
	
	/**
	 * Valida a compra com o bilhete nulo
	 */
	@Test(expected = BSIException.class)
	public void validarCompraSemBilhete(){
		//Atribui null ao bilhete
		this.bilhete = null;
		
		//Realiza a acao de Check In
		this.realizarAcaoComprar();
	}
	
	/**
	 * Valida o bilhete em outra situacao que nao a reservado
	 */
	@Test(expected = BSIException.class)
	public void validarBilheteSemSituacaoDisponivel(){
		//Muda a situacao do bilhete
		this.bilhete.setSituacaoEnum(SituacaoBilheteEnum.DISPONIVEL);
		
		//Realiza a acao de Comprar
		this.realizarAcaoComprar();
	}	
}
