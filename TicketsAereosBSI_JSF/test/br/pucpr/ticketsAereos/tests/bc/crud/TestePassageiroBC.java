package br.pucpr.ticketsAereos.tests.bc.crud;

import java.util.Date;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.PassageiroBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TestePassageiroModel;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TestePassageiroBC extends TestePassageiroModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TestePassageiroBC(MassaTestesPassageiroEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida Passageiro completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Passageiro nulo
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroNulo(){
		PassageiroBC.getInstance().insert(null);
	}
	
	/**
	 * Valida Passageiro preenchido com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroEspacosBranco(){
		this.object.setDocumento("                         ");
		this.object.setEmail("                         ");
		this.object.setNome("                         ");
		this.object.setNumeroCartao("                         ");
		this.object.setTelefone("                         ");

		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Passageiro sem o campo documento
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemDocumento(){
		this.object.setDocumento(null);
		PassageiroBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Passageiro sem o campo data nascimento
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemDataNascimento(){
		this.object.setDataNascimento(null);
		PassageiroBC.getInstance().insert(this.object);
	}		
	
	/**
	 * Valida o funcionario com o campo data nascimento apos a data atual
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroComDataNascimentoAposDataAtual(){
		//Cria uma data com 30 dias a frente da data atual
		Date dataPlus30 = DateUtils.datePlusDays(new Date(), 30);
		this.object.setDataNascimento(dataPlus30);
		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Passageiro sem o campo email
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemEmail(){
		this.object.setEmail(null);
		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Passageiro com o campo email sem o @
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroComEmailSemArroba(){
		this.object.setEmail("func1ticketeventosbsi.com.br");
		PassageiroBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Passageiro com o campo email sem o . após o @
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroComEmailSemPonto(){
		this.object.setEmail("func.1@ticketeventosbsi");
		PassageiroBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Passageiro sem o campo nome
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemNome(){
		this.object.setNome(null);
		PassageiroBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Passageiro sem o campo numero cartao
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemNumeroCartao(){
		this.object.setNumeroCartao(null);
		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Passageiro sem o campo telefone
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroSemTelefone(){
		this.object.setTelefone(null);
		PassageiroBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida o Passageiro com endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarPassageiroComEnderecoNulo(){
		this.object.setEndereco(null);
		PassageiroBC.getInstance().insert(this.object);
	}	
}
