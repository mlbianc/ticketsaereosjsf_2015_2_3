package br.pucpr.ticketsAereos.tests.bc.crud;

import java.util.Date;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.HorarioBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteHorarioModel;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteHorarioBC extends TesteHorarioModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteHorarioBC(MassaTestesHorarioEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida horario completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario nulo
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioNulo(){
		HorarioBC.getInstance().insert(null);
	}
	
	/**
	 * Valida horario preenchido com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioEspacosBranco(){
		object.setCodigo("                         ");

		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario sem o campo chegada
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioSemChegada(){
		object.setChegada(null);
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario com o campo chegada antes da Partida
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioComChegadaAntesPartida(){
		Date dataMinus30 = DateUtils.datePlusDays(object.getPartida(), -30);
		object.setChegada(dataMinus30);
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario sem o campo codigo
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioSemCodigo(){
		object.setCodigo(null);
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario sem o campo partida
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioSemPartida(){
		object.setPartida(null);
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario com o campo partida antes da data atual
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioComPartidaAntesDataAtual(){
		Date dataMinus30 = DateUtils.datePlusDays(new Date(), -30);
		object.setChegada(dataMinus30);
		HorarioBC.getInstance().insert(object);
	}		
	
	/**
	 * Valida horario com a qtd economica negativa
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioQtdEconomicaNegativa(){
		object.setQtdEconomica(-20);
		HorarioBC.getInstance().insert(object);
	}
	
	/**
	 * Valida horario com a qtd executiva negativa
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioQtdExecutivaNegativa(){
		object.setQtdExecutiva(-20);
		HorarioBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida horario com a qtd primeira negativa
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioQtdPrimeiraNegativa(){
		object.setQtdPrimeira(-20);
		HorarioBC.getInstance().insert(object);
	}		
	
	/**
	 * Valida o horario com rota nula
	 */
	@Test(expected = BSIException.class)
	public void validarHorarioSemRota(){
		object.setRota(null);
		HorarioBC.getInstance().insert(object);
	}	
}
