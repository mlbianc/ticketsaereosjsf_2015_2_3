package br.pucpr.ticketsAereos.tests.bc.crud;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.AeroportoBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteAeroportoModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteAeroportoBC extends TesteAeroportoModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteAeroportoBC(MassaTestesAeroportoEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida aeroporto completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		AeroportoBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida aeroporto nulo
	 */
	@Test(expected = BSIException.class)
	public void validarAeroportoNulo(){
		AeroportoBC.getInstance().insert(null);
	}
	
	/**
	 * Valida aeroporto preenchido com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarAeroportoEspacosBranco(){
		object.setNome("                         ");
		object.setCodigo("                         ");

		AeroportoBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida aeroporto sem o campo nome
	 */
	@Test(expected = BSIException.class)
	public void validarAeroportoSemNome(){
		object.setNome(null);
		AeroportoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida aeroporto sem o campo codigo
	 */
	@Test(expected = BSIException.class)
	public void validarAeroportoSemCodigo(){
		object.setCodigo(null);
		AeroportoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida o aeroporto com endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarAeroportoComEnderecoNulo(){
		object.setEndereco(null);
		AeroportoBC.getInstance().insert(object);
	}	
}
