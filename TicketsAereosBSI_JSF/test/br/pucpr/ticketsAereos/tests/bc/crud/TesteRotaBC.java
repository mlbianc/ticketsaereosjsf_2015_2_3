package br.pucpr.ticketsAereos.tests.bc.crud;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.RotaBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteRotaModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteRotaBC extends TesteRotaModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteRotaBC(MassaTestesRotaEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida Rota completa
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		RotaBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Rota nula
	 */
	@Test(expected = BSIException.class)
	public void validarRotaNula(){
		RotaBC.getInstance().insert(null);
	}
	
	/**
	 * Valida Rota preenchida com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarRotaEspacosBranco(){
		this.object.setNome("                         ");
		this.object.setDescricao("                         ");

		RotaBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Rota sem o campo cia aerea
	 */
	@Test(expected = BSIException.class)
	public void validarRotaSemCiaAerea(){
		this.object.setCiaAerea(null);
		RotaBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Rota sem o campo descricao
	 */
	@Test(expected = BSIException.class)
	public void validarRotaSemDescricao(){
		this.object.setDescricao(null);
		RotaBC.getInstance().insert(this.object);
	}	
	
	
	/**
	 * Valida Rota sem o campo destino
	 */
	@Test(expected = BSIException.class)
	public void validarRotaSemDestino(){
		this.object.setDestino(null);
		RotaBC.getInstance().insert(this.object);
	}		
	
	/**
	 * Valida Rota sem o campo origem
	 */
	@Test(expected = BSIException.class)
	public void validarRotaSemOrigem(){
		this.object.setOrigem(null);
		RotaBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Rota com o campo origem e destino iguais
	 */
	@Test(expected = BSIException.class)
	public void validarRotaComOrigemDestinoIguais(){
		//Dica implemente o m�todo equals na classe Aeroporto e compare com os codigos do aeroporto
		this.object.setDestino(this.object.getOrigem());
		RotaBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Rota sem o campo nome
	 */
	@Test(expected = BSIException.class)
	public void validarRotaSemNome(){
		this.object.setNome(null);
		RotaBC.getInstance().insert(this.object);
	}
}
