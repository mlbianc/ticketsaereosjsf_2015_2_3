package br.pucpr.ticketsAereos.tests.bc.crud;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.EnderecoBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteEnderecoModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteEnderecoBC extends TesteEnderecoModel{
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteEnderecoBC(MassaTestesEnderecoEnum enumm) {
		super(enumm);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um endereco completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoNulo(){
		EnderecoBC.getInstance().insert(null);
	}
	
	/**
	 * Valida endereco com o nome preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoEspacosBranco(){
		object.setBairro("                         ");
		object.setCidade("                         ");
		object.setComplemento("                    ");
		object.setEstado("                         ");
		object.setPais("                           ");
		object.setRua("                            ");

		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem o nome da rua
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemRua(){
		object.setRua(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem o numero
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemNumero(){
		object.setNumero(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco com o numero negativo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoComNumeroNegativo(){
		object.setNumero(-200);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem complemento
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemComplemento(){
		object.setComplemento(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem bairro
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemBairro(){
		object.setBairro(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem cidade
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemCidade(){
		object.setCidade(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem estado
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemEstado(){
		object.setEstado(null);
		EnderecoBC.getInstance().insert(object);
	}
	
	/**
	 * Valida endereco sem pais
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemPais(){
		object.setPais(null);
		EnderecoBC.getInstance().insert(object);
	}
}
