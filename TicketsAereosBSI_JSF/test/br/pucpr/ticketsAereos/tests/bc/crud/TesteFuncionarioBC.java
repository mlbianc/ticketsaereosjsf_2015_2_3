package br.pucpr.ticketsAereos.tests.bc.crud;

import java.util.Date;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.FuncionarioBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteFuncionarioModel;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteFuncionarioBC extends TesteFuncionarioModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteFuncionarioBC(MassaTestesFuncionarioEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida Funcionario completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Funcionario nulo
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioNulo(){
		FuncionarioBC.getInstance().insert(null);
	}
	
	/**
	 * Valida Funcionario preenchido com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioEspacosBranco(){
		this.object.setCodigo("                         ");
		this.object.setContaCorrente("                         ");
		this.object.setEmail("                         ");
		this.object.setNome("                         ");
		this.object.setTelefone("                         ");

		FuncionarioBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Funcionario sem o campo codigo
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemCodigo(){
		this.object.setCodigo(null);
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Funcionario sem o campo conta corrente
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemContaCorrente(){
		this.object.setContaCorrente(null);
		FuncionarioBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Funcionario sem o campo data nascimento
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemDataNascimento(){
		this.object.setDataNascimento(null);
		FuncionarioBC.getInstance().insert(this.object);
	}		
	
	/**
	 * Valida o funcionario com o campo data nascimento apos a data atual
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioComDataNascimentoAposDataAtual(){
		//Cria uma data com 30 dias a frente da data atual
		Date dataPlus30 = DateUtils.datePlusDays(new Date(), 30);
		this.object.setDataNascimento(dataPlus30);
		FuncionarioBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Funcionario sem o campo email
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemEmail(){
		this.object.setEmail(null);
		FuncionarioBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida Funcionario com o campo email sem o @
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioComEmailSemArroba(){
		this.object.setEmail("func1ticketeventosbsi.com.br");
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Funcionario com o campo email sem o . após o @
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioComEmailSemPonto(){
		this.object.setEmail("func.1@ticketeventosbsi");
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Funcionario sem o campo nome
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemNome(){
		this.object.setNome(null);
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida Funcionario sem o campo telefone
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioSemTelefone(){
		this.object.setTelefone(null);
		FuncionarioBC.getInstance().insert(this.object);
	}	
	
	/**
	 * Valida o Funcionario com endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioComEnderecoNulo(){
		this.object.setEndereco(null);
		FuncionarioBC.getInstance().insert(this.object);
	}
	
	/**
	 * Valida o Funcionario com cia aerea nula
	 */
	@Test(expected = BSIException.class)
	public void validarFuncionarioComCiaAereaNula(){
		this.object.setCiaAerea(null);
		FuncionarioBC.getInstance().insert(this.object);
	}	
}
