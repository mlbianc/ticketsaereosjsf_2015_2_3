package br.pucpr.ticketsAereos.tests.model.actions;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Bagagem;
import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.Horario;
import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.model.enums.TipoBagagemEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCheckInBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.tests.modificadores.Creator;
import br.pucpr.ticketsAereos.tests.modificadores.HorarioCreator;
import br.pucpr.ticketsAereos.tests.modificadores.PassageiroCreator;
import br.pucpr.ticketsAereos.tests.verificador.BagagemVerificator;
import br.pucpr.ticketsAereos.tests.verificador.BilheteVerificator;
import br.pucpr.ticketsAereos.tests.verificador.HorarioVerificator;
import br.pucpr.ticketsAereos.tests.verificador.PassageiroVerificator;
import br.pucpr.ticketsAereos.tests.verificador.Verificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */
@RunWith(Parameterized.class)
public class TesteCheckInBilheteModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected MassaTestesCheckInBilheteEnum checkInBilheteEnum;
	protected Horario horario;
	protected Bilhete bilhete;
	protected Passageiro passageiro;
	protected Creator<Horario, MassaTestesHorarioEnum> creator;
	protected Verificator<Horario, MassaTestesHorarioEnum> verificator;
	protected String assento;
	protected TipoBagagemEnum tipoBagagem;
	protected Double peso;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesCheckInBilheteEnum.getParameters();
	}	
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteCheckInBilheteModel(MassaTestesCheckInBilheteEnum checkInBilheteEnum) {
		System.out.println("\t\tValidando: " + checkInBilheteEnum);
		this.checkInBilheteEnum = checkInBilheteEnum;
		this.creator = HorarioCreator.getInstance();
		this.verificator = HorarioVerificator.getInstance();
	}
	
	//////////////////////////////////////////
	// Metodos Utilitarios
	//////////////////////////////////////////
	/**
	 * Metodo que realiza a acao de Reservar na model
	 */
	protected void realizarAcaoReservar(){
		//Realiza a reserva do Bilhete
		this.bilhete.reservar(this.passageiro, this.assento);
	}
	
	/**
	 * Metodo que realiza a acao de Comprar na model
	 */
	protected void realizarAcaoComprar(){
		//Realiza a comprar do Bilhete
		this.bilhete.comprar();
	}	

	/**
	 * Metodo que realiza a acao de CheckIn na model
	 */
	protected Bagagem realizarAcaoCheckIn(){
		//Realiza o checkIn da bagagem do Bilhete
		return this.bilhete.checkIn(this.tipoBagagem, this.peso);
	}
	
	@Before
	public void init() {
		//Aqui as coisas mudam um pouco pois os bilhetes devem ser criados automaticamente na criacao do horario
		this.horario = this.creator.create(this.checkInBilheteEnum.getBilheteEnum().getHorarioEnum());
		// Verifica se os atributos estao preenchidos
		this.verificator.verify(this.horario);
		
		switch (this.checkInBilheteEnum.getBilheteEnum().getTipoBilheteEnum()) {
		case ECONOMICA:
			this.bilhete = this.horario.getEconomicas().get(0);
			break;
		case EXECUTIVA:
			this.bilhete = this.horario.getExecutivas().get(0);
			break;
		case PRIMEIRA:
			this.bilhete = this.horario.getPrimeiras().get(0);
		default:
			break;
		}
		// Verifica se o bilhete esta preenchido
		BilheteVerificator.getInstance().verify(this.bilhete);
		
		//criando o passageiro
		this.passageiro = PassageiroCreator.getInstance().create(this.checkInBilheteEnum.getBilheteEnum().getPassageiroEnum());
		
		//Obtem o Assento do Enum
		this.assento = this.checkInBilheteEnum.getBilheteEnum().getAssento();
		
		//Realiza a acao do teste
		this.realizarAcaoReservar();
		
		//Verifica se a situacao do bilhete ficou como reservada
		Assert.assertNotNull(this.bilhete.getSituacaoEnum());
		Assert.assertEquals(this.bilhete.getSituacaoEnum(), SituacaoBilheteEnum.RESERVADO);

		//Verifica se o passageiro foi setado no bilhete e se este passou a ter assento
		Assert.assertNotNull(this.bilhete.getPassageiro());
		PassageiroVerificator.getInstance().verify(this.bilhete.getPassageiro());
		Assert.assertTrue(StringUtils.isNotBlank(this.bilhete.getAssento()));
		
		//Realiza a acao de Comprar o bilhete
		this.realizarAcaoComprar();
		
		//Verifica se a situacao do bilhete ficou como comprado
		Assert.assertNotNull(this.bilhete.getSituacaoEnum());
		Assert.assertEquals(this.bilhete.getSituacaoEnum(), SituacaoBilheteEnum.COMPRADO);
		
		//Obtem o Tipo da Bagagem
		this.tipoBagagem = this.checkInBilheteEnum.getBagagemEnum().getTipoBagagemEnum();
		
		//Obtem o peso da Bagagem 
		this.peso = this.checkInBilheteEnum.getBagagemEnum().getPeso();
	}
	
	@Test
	public void criar() {
		//Realiza o checkIn da bagagem do Bilhete
		Bagagem bagagem = this.realizarAcaoCheckIn();
		
		//Verifica se a situacao do bilhete ficou como checkin
		Assert.assertNotNull(this.bilhete.getSituacaoEnum());
		Assert.assertEquals(this.bilhete.getSituacaoEnum(), this.checkInBilheteEnum.getSituacaoEnum());
		
		//Verifica se a bagagem esta correta
		BagagemVerificator.getInstance().verify(bagagem, this.checkInBilheteEnum.getBagagemEnum());
	}	
}
