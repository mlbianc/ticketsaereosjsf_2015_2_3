package br.pucpr.ticketsAereos.tests.model.actions;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.Horario;
import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCancelarReservaBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.tests.modificadores.Creator;
import br.pucpr.ticketsAereos.tests.modificadores.HorarioCreator;
import br.pucpr.ticketsAereos.tests.modificadores.PassageiroCreator;
import br.pucpr.ticketsAereos.tests.verificador.BilheteVerificator;
import br.pucpr.ticketsAereos.tests.verificador.HorarioVerificator;
import br.pucpr.ticketsAereos.tests.verificador.PassageiroVerificator;
import br.pucpr.ticketsAereos.tests.verificador.Verificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */
@RunWith(Parameterized.class)
public class TesteCancelarReservaBilheteModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected MassaTestesCancelarReservaBilheteEnum cancelarBilheteEnum;
	protected Horario horario;
	protected Bilhete bilhete;
	protected Passageiro passageiro;
	protected Creator<Horario, MassaTestesHorarioEnum> creator;
	protected Verificator<Horario, MassaTestesHorarioEnum> verificator;
	protected String assento;
	
	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesCancelarReservaBilheteEnum.getParameters();
	}	
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteCancelarReservaBilheteModel(MassaTestesCancelarReservaBilheteEnum cancelarBilheteEnum) {
		System.out.println("\t\tValidando: " + cancelarBilheteEnum);
		this.cancelarBilheteEnum = cancelarBilheteEnum;
		this.creator = HorarioCreator.getInstance();
		this.verificator = HorarioVerificator.getInstance();
	}
	
	//////////////////////////////////////////
	// Metodos Utilitarios
	//////////////////////////////////////////
	/**
	 * Metodo que realiza a acao de Reservar na model
	 */
	protected void realizarAcaoReservar(){
		//Realiza a reserva do Bilhete
		this.bilhete.reservar(this.passageiro, this.assento);
	}
	
	/**
	 * Metodo que realiza a acao de Cancelar na model
	 */
	protected void realizarAcaoCancelarReserva(){
		//Realiza a comprar do Bilhete
		this.bilhete.cancelarReserva();
	}	
	
	@Before
	public void init() {
		//Aqui as coisas mudam um pouco pois os bilhetes devem ser criados automaticamente na criacao do horario
		this.horario = this.creator.create(this.cancelarBilheteEnum.getBilheteEnum().getHorarioEnum());
		// Verifica se os atributos estao preenchidos
		this.verificator.verify(this.horario);
		
		switch (this.cancelarBilheteEnum.getBilheteEnum().getTipoBilheteEnum()) {
		case ECONOMICA:
			this.bilhete = this.horario.getEconomicas().get(0);
			break;
		case EXECUTIVA:
			this.bilhete = this.horario.getExecutivas().get(0);
			break;
		case PRIMEIRA:
			this.bilhete = this.horario.getPrimeiras().get(0);
		default:
			break;
		}
		// Verifica se o bilhete esta preenchido
		BilheteVerificator.getInstance().verify(this.bilhete);
		
		//criando o passageiro
		this.passageiro = PassageiroCreator.getInstance().create(this.cancelarBilheteEnum.getBilheteEnum().getPassageiroEnum());
		
		//Obtem o Assento do Enum
		this.assento = this.cancelarBilheteEnum.getBilheteEnum().getAssento();
		
		//Realiza a acao do teste
		this.realizarAcaoReservar();
		
		//Verifica se a situacao do bilhete ficou como reservada
		Assert.assertNotNull(this.bilhete.getSituacaoEnum());
		Assert.assertEquals(this.bilhete.getSituacaoEnum(), SituacaoBilheteEnum.RESERVADO);

		//Verifica se o passageiro foi setado no bilhete e se este passou a ter assento
		Assert.assertNotNull(this.bilhete.getPassageiro());
		PassageiroVerificator.getInstance().verify(this.bilhete.getPassageiro());
		Assert.assertTrue(StringUtils.isNotBlank(this.bilhete.getAssento()));
	}

	
	@Test
	public void criar() {
		//Realiza o cancelamento da reserva do Bilhete
		this.realizarAcaoCancelarReserva();
		
		//Verifica se a situacao do bilhete ficou como disponivel
		Assert.assertNotNull(this.bilhete.getSituacaoEnum());
		Assert.assertEquals(this.bilhete.getSituacaoEnum(), this.cancelarBilheteEnum.getSituacaoEnum());
		
		//Verifica se o passageiro foi retirado do bilhete, assim como o assento
		Assert.assertNull(this.bilhete.getPassageiro());
		Assert.assertNull(this.bilhete.getAssento());
	}	
}
