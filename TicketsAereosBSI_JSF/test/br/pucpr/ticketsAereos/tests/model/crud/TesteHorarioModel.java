package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Horario;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.tests.modificadores.HorarioCreator;
import br.pucpr.ticketsAereos.tests.verificador.HorarioVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */
@RunWith(Parameterized.class)
public class TesteHorarioModel extends AbstractCrudTestModel<Horario, MassaTestesHorarioEnum> {

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesHorarioEnum.getParameters();
	}		
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteHorarioModel(MassaTestesHorarioEnum horarioEnum) {
		super(horarioEnum);
		this.creator = HorarioCreator.getInstance();
		this.verificator = HorarioVerificator.getInstance();
		this.objectEnumTemp = horarioEnum;
	}
	
	
	@Override
	@Test
	public void criar() {
		super.criar();
		
		//TODO testar os bilhetes
	}
}
