package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;
import br.pucpr.ticketsAereos.tests.modificadores.AeroportoCreator;
import br.pucpr.ticketsAereos.tests.verificador.AeroportoVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteAeroportoModel extends AbstractCrudTestModel<Aeroporto, MassaTestesAeroportoEnum>  {

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesAeroportoEnum.getParameters();
	}

	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteAeroportoModel(MassaTestesAeroportoEnum aeroportoEnum) {
		super(aeroportoEnum);
		creator = AeroportoCreator.getInstance();
		verificator = AeroportoVerificator.getInstance();
		objectEnumTemp = MassaTestesAeroportoEnum.getInstance();
	}
}
