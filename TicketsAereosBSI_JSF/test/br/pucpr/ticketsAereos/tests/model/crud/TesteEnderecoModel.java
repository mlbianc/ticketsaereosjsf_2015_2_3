package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;
import br.pucpr.ticketsAereos.tests.modificadores.EnderecoCreator;
import br.pucpr.ticketsAereos.tests.verificador.EnderecoVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

@RunWith(Parameterized.class)
public class TesteEnderecoModel extends AbstractCrudTestModel<Endereco, MassaTestesEnderecoEnum>  {
	
	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesEnderecoEnum.getParameters();
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteEnderecoModel(MassaTestesEnderecoEnum enderecoEnum) {
		super(enderecoEnum);
		creator = EnderecoCreator.getInstance();
		verificator = EnderecoVerificator.getInstance();
		objectEnumTemp = MassaTestesEnderecoEnum.getInstance();
	}
}
