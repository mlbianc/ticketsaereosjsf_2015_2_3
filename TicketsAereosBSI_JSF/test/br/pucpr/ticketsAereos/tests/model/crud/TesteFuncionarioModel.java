package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Funcionario;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.tests.modificadores.FuncionarioCreator;
import br.pucpr.ticketsAereos.tests.verificador.FuncionarioVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteFuncionarioModel extends AbstractCrudTestModel<Funcionario, MassaTestesFuncionarioEnum> {

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesFuncionarioEnum.getParameters();
	}		
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteFuncionarioModel(MassaTestesFuncionarioEnum funcionarioEnum) {
		super(funcionarioEnum);
		creator = FuncionarioCreator.getInstance();
		verificator = FuncionarioVerificator.getInstance();
		objectEnumTemp = MassaTestesFuncionarioEnum.getInstance();	
	}
}
