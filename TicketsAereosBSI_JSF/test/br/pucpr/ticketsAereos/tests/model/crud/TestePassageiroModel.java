package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.tests.modificadores.PassageiroCreator;
import br.pucpr.ticketsAereos.tests.verificador.PassageiroVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TestePassageiroModel extends AbstractCrudTestModel<Passageiro, MassaTestesPassageiroEnum> {

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesPassageiroEnum.getParameters();
	}
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TestePassageiroModel(MassaTestesPassageiroEnum funcionarioEnum) {
		super(funcionarioEnum);
		creator = PassageiroCreator.getInstance();
		verificator = PassageiroVerificator.getInstance();
		objectEnumTemp = MassaTestesPassageiroEnum.getInstance();	
	}
}
