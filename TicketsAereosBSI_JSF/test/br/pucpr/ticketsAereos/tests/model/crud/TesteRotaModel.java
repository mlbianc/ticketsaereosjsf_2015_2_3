package br.pucpr.ticketsAereos.tests.model.crud;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;
import br.pucpr.ticketsAereos.tests.modificadores.RotaCreator;
import br.pucpr.ticketsAereos.tests.verificador.RotaVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */
@RunWith(Parameterized.class)
public class TesteRotaModel extends AbstractCrudTestModel<Rota, MassaTestesRotaEnum> {

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return MassaTestesRotaEnum.getParameters();
	}
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public TesteRotaModel(MassaTestesRotaEnum aviaoEnum) {
		super(aviaoEnum);
		creator = RotaCreator.getInstance();
		verificator = RotaVerificator.getInstance();
		objectEnumTemp = MassaTestesRotaEnum.getInstance();
	}
}
