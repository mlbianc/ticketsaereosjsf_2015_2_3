package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Aeroporto;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesAeroportoEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class AeroportoVerificator extends Verificator<Aeroporto, MassaTestesAeroportoEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static AeroportoVerificator instance = new AeroportoVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	private AeroportoVerificator() {	
	}
	
	public static AeroportoVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	public void verify(Aeroporto aeroporto){
		super.verify(aeroporto);
		
		Assert.assertTrue(StringUtils.isNotBlank(aeroporto.getCodigo()));
		Assert.assertTrue(StringUtils.isNotBlank(aeroporto.getNome()));
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(aeroporto.getEndereco());
	}	
	
	public void verify(Aeroporto aeroporto, MassaTestesAeroportoEnum aeroportoEnum){
		super.verify(aeroporto, aeroportoEnum);
		
		//Verifica pontos base
		verify(aeroporto);
		
		//Verifica atributos comuns
		Assert.assertEquals(aeroporto.getNome(), 								aeroportoEnum.getNome());
		Assert.assertEquals(aeroporto.getCodigo(),					 			aeroportoEnum.getCodigo());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(aeroporto.getEndereco(), 		aeroportoEnum.getEnderecoEnum());
	}	
	
	public void verify(Aeroporto aeroportoBD, Aeroporto aeroporto){
		super.verify(aeroportoBD, aeroporto);
		
		//Verifica pontos base
		verify(aeroporto);
		verify(aeroportoBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(aeroportoBD.getCodigo(), 							aeroporto.getCodigo());
		Assert.assertEquals(aeroportoBD.getNome(),								aeroporto.getNome());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(aeroportoBD.getEndereco(), 	aeroporto.getEndereco());
	}
}
