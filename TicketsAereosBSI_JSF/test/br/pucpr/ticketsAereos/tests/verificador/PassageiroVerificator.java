package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Passageiro;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesPassageiroEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class PassageiroVerificator extends Verificator<Passageiro, MassaTestesPassageiroEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static PassageiroVerificator instance = new PassageiroVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private PassageiroVerificator() {		
	}	
	
	public static PassageiroVerificator getInstance() {
		return instance;
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////

	@Override
	public void verify(Passageiro passageiro){
		super.verify(passageiro);
		
		Assert.assertTrue(StringUtils.isNotBlank(passageiro.getEmail()));
		Assert.assertTrue(StringUtils.isNotBlank(passageiro.getNome()));
		Assert.assertTrue(StringUtils.isNotBlank(passageiro.getTelefone()));
		Assert.assertNotNull(passageiro.getDataNascimento());
		Assert.assertTrue(StringUtils.isNotBlank(passageiro.getDocumento()));
		Assert.assertTrue(StringUtils.isNotBlank(passageiro.getNumeroCartao()));
		
		EnderecoVerificator.getInstance().verify(passageiro.getEndereco());
	}	

	@Override
	public void verify(Passageiro passageiro, MassaTestesPassageiroEnum passageiroEnum){
		super.verify(passageiro, passageiroEnum);
		
		//Verifica pontos base
		this.verify(passageiro);
		
		//Verifica atributos comuns
		Assert.assertEquals(DateUtils.minimizeDate(passageiro.getDataNascimento()), 	
							DateUtils.minimizeDate(passageiroEnum.getDataNascimento()));		
		
		Assert.assertEquals(passageiro.getEmail(), 							passageiroEnum.getEmail());
		Assert.assertEquals(passageiro.getNome(), 							passageiroEnum.getNome());
		Assert.assertEquals(passageiro.getTelefone(), 						passageiroEnum.getTelefone());
		Assert.assertEquals(passageiro.getDocumento(), 						passageiroEnum.getDocumento());
		Assert.assertEquals(passageiro.getNumeroCartao(),				 	passageiroEnum.getNumeroCartao());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(passageiro.getEndereco(),	passageiroEnum.getEnderecoEnum());		
	}	

	@Override
	public void verify(Passageiro passageiroBD, Passageiro passageiro){
		super.verify(passageiroBD, passageiro);
		
		//Verifica pontos base
		this.verify(passageiro);
		this.verify(passageiroBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(DateUtils.minimizeDate(passageiroBD.getDataNascimento()), 	
							DateUtils.minimizeDate(passageiro.getDataNascimento()));
		
		Assert.assertEquals(passageiroBD.getEmail(),			 			passageiro.getEmail());
		Assert.assertEquals(passageiroBD.getNome(),							passageiro.getNome());
		Assert.assertEquals(passageiroBD.getTelefone(), 					passageiro.getTelefone());
		Assert.assertEquals(passageiroBD.getDocumento(), 					passageiro.getDocumento());
		Assert.assertEquals(passageiroBD.getNumeroCartao(), 				passageiro.getNumeroCartao());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(passageiroBD.getEndereco(),passageiro.getEndereco());
	}
}
