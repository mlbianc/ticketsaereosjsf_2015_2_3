package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Endereco;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesEnderecoEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class EnderecoVerificator extends Verificator<Endereco, MassaTestesEnderecoEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static EnderecoVerificator instance = new EnderecoVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private EnderecoVerificator() {	
	}	
	
	public static EnderecoVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	public void verify(Endereco endereco){
		super.verify(endereco);
		
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getBairro()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getCidade()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getComplemento()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getEstado()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getPais()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getRua()));
		Assert.assertNotNull(endereco.getNumero());
		Assert.assertFalse(endereco.getNumero() < 0);
	}	
	
	public void verify(Endereco endereco, MassaTestesEnderecoEnum enderecoEnum){
		super.verify(endereco, enderecoEnum);
		
		//Verifica pontos base
		verify(endereco);
		
		//Verifica atributos comuns
		Assert.assertEquals(endereco.getBairro(), enderecoEnum.getBairro());
		Assert.assertEquals(endereco.getCidade(), enderecoEnum.getCidade());
		Assert.assertEquals(endereco.getComplemento(), enderecoEnum.getComplemento());
		Assert.assertEquals(endereco.getEstado(), enderecoEnum.getEstado());
		Assert.assertEquals(endereco.getNumero(), enderecoEnum.getNumero(), 0);
		Assert.assertEquals(endereco.getPais(), enderecoEnum.getPais());
		Assert.assertEquals(endereco.getRua(), enderecoEnum.getRua());
	}	
	
	public void verify(Endereco enderecoBD, Endereco endereco){
		super.verify(enderecoBD, endereco);
		
		//Verifica pontos base
		verify(endereco);
		verify(enderecoBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(enderecoBD.getBairro(), 		endereco.getBairro());
		Assert.assertEquals(enderecoBD.getCidade(), 		endereco.getCidade());
		Assert.assertEquals(enderecoBD.getComplemento(), 	endereco.getComplemento());
		Assert.assertEquals(enderecoBD.getEstado(), 		endereco.getEstado());
		Assert.assertEquals(enderecoBD.getNumero(),			endereco.getNumero());
		Assert.assertEquals(enderecoBD.getPais(), 			endereco.getPais());
		Assert.assertEquals(enderecoBD.getRua(), 			endereco.getRua());
	}
}
