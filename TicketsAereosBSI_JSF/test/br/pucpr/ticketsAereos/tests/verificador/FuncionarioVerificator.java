package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Funcionario;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesFuncionarioEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class FuncionarioVerificator extends Verificator<Funcionario, MassaTestesFuncionarioEnum>{

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static FuncionarioVerificator instance = new FuncionarioVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private FuncionarioVerificator() {		
	}	
	
	public static FuncionarioVerificator getInstance() {
		return instance;
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////

	@Override
	public void verify(Funcionario funcionario){
		super.verify(funcionario);
		
		Assert.assertTrue(StringUtils.isNotBlank(funcionario.getEmail()));
		Assert.assertTrue(StringUtils.isNotBlank(funcionario.getNome()));
		Assert.assertTrue(StringUtils.isNotBlank(funcionario.getTelefone()));
		Assert.assertNotNull(funcionario.getDataNascimento());
		Assert.assertTrue(StringUtils.isNotBlank(funcionario.getCodigo()));
		Assert.assertTrue(StringUtils.isNotBlank(funcionario.getContaCorrente()));
		
		EnderecoVerificator.getInstance().verify(funcionario.getEndereco());
		CiaAereaVerificator.getInstance().verify(funcionario.getCiaAerea());
	}	

	@Override
	public void verify(Funcionario funcionario, MassaTestesFuncionarioEnum funcionarioEnum){
		super.verify(funcionario, funcionarioEnum);
		
		//Verifica pontos base
		this.verify(funcionario);
		
		//Verifica atributos comuns
		Assert.assertEquals(DateUtils.minimizeDate(funcionario.getDataNascimento()), 	
							DateUtils.minimizeDate(funcionarioEnum.getDataNascimento()));
		
		Assert.assertEquals(funcionario.getEmail(), 						funcionarioEnum.getEmail());
		Assert.assertEquals(funcionario.getNome(), 							funcionarioEnum.getNome());
		Assert.assertEquals(funcionario.getTelefone(), 						funcionarioEnum.getTelefone());
		Assert.assertEquals(funcionario.getCodigo(), 						funcionarioEnum.getCodigo());
		Assert.assertEquals(funcionario.getContaCorrente(), 				funcionarioEnum.getContaCorrente());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(funcionario.getEndereco(),	funcionarioEnum.getEnderecoEnum());
		
		//Verifica a ciaAerea
		CiaAereaVerificator.getInstance().verify(funcionario.getCiaAerea(), funcionarioEnum.getCiaAerea());
	}	

	@Override
	public void verify(Funcionario funcionarioBD, Funcionario funcionario){
		super.verify(funcionarioBD, funcionario);
		
		//Verifica pontos base
		this.verify(funcionario);
		this.verify(funcionarioBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(DateUtils.minimizeDate(funcionarioBD.getDataNascimento()), 	
							DateUtils.minimizeDate(funcionario.getDataNascimento()));

		Assert.assertEquals(funcionarioBD.getEmail(),				 			funcionario.getEmail());
		Assert.assertEquals(funcionarioBD.getNome(),							funcionario.getNome());
		Assert.assertEquals(funcionarioBD.getTelefone(), 						funcionario.getTelefone());
		Assert.assertEquals(funcionarioBD.getCodigo(), 							funcionario.getCodigo());
		Assert.assertEquals(funcionarioBD.getContaCorrente(), 					funcionario.getContaCorrente());
		
		//Verifica o endereco
		EnderecoVerificator.getInstance().verify(funcionarioBD.getEndereco(),	funcionario.getEndereco());
		
		//Verifica a ciaAerea
		CiaAereaVerificator.getInstance().verify(funcionario.getCiaAerea(), funcionario.getCiaAerea());
	}
}
