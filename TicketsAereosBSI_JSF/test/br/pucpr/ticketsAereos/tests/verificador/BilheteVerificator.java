package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.enums.SituacaoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBilheteEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class BilheteVerificator extends Verificator<Bilhete, MassaTestesBilheteEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static BilheteVerificator instance = new BilheteVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private BilheteVerificator() {	
	}
	
	public static BilheteVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Override
	public void verify(Bilhete bilhete){
		super.verify(bilhete);
		
		Assert.assertNotNull(bilhete.getTipoBilheteEnum());
		Assert.assertNotNull(bilhete.getSituacaoEnum());
		
		if(bilhete.getSituacaoEnum().equals(SituacaoBilheteEnum.DISPONIVEL)){
			Assert.assertNull(bilhete.getPassageiro());
			Assert.assertTrue(StringUtils.isBlank(bilhete.getAssento()));
		} else {
			PassageiroVerificator.getInstance().verify(bilhete.getPassageiro());
			Assert.assertTrue(StringUtils.isNotBlank(bilhete.getAssento()));
			
			//Verifica a associacao bidirecional com Pessoa
			Assert.assertTrue(bilhete.getPassageiro().getBilhetes().contains(bilhete));
		}
		
		//Verifica o horario de um bilhete
		HorarioVerificator.getInstance().verify(bilhete.getHorario());
	}	
	
	@Override
	public void verify(Bilhete bilhete, MassaTestesBilheteEnum bilheteEnum){
		super.verify(bilhete, bilheteEnum);
		
		//Verifica pontos base
		this.verify(bilhete);
		
		//Verifica atributos comuns
		Assert.assertEquals(bilhete.getTipoBilheteEnum(), 						bilheteEnum.getTipoBilheteEnum());
		Assert.assertEquals(bilhete.getSituacaoEnum(), 							bilheteEnum.getSituacaoEnum());
		
		//Senao for a situacao disponivel
		if(!bilhete.getSituacaoEnum().equals(SituacaoBilheteEnum.DISPONIVEL)){
			PassageiroVerificator.getInstance().verify(bilhete.getPassageiro(),		bilheteEnum.getPassageiroEnum());
			Assert.assertEquals(bilhete.getAssento(), 							bilheteEnum.getAssento());
		}
		
		HorarioVerificator.getInstance().verify(bilhete.getHorario(),			bilheteEnum.getHorarioEnum());
	}	
	
	@Override
	public void verify(Bilhete bilheteBD, Bilhete bilhete){
		super.verify(bilheteBD, bilhete);
		
		//Verifica pontos base
		this.verify(bilhete);
		this.verify(bilheteBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(bilheteBD.getTipoBilheteEnum(), 					bilhete.getTipoBilheteEnum());
		Assert.assertEquals(bilheteBD.getSituacaoEnum(), 						bilhete.getSituacaoEnum());
		
		HorarioVerificator.getInstance().verify(bilheteBD.getHorario(),			bilhete.getHorario());
		
		if(!bilhete.getSituacaoEnum().equals(SituacaoBilheteEnum.DISPONIVEL)){
			PassageiroVerificator.getInstance().verify(bilhete.getPassageiro(),		bilhete.getPassageiro());	
		}
	}
}
