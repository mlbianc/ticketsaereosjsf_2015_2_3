package br.pucpr.ticketsAereos.tests.verificador;

import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Bagagem;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesBagagemEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class BagagemVerificator extends Verificator<Bagagem, MassaTestesBagagemEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static BagagemVerificator instance = new BagagemVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private BagagemVerificator() {	
	}
	
	public static BagagemVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	public void verify(Bagagem bagagem){
		super.verify(bagagem);
		
		BilheteVerificator.getInstance().verify(bagagem.getBilhete());
		
		//Verifica a associacao bidirecional com bilhete
		Assert.assertTrue(bagagem.getBilhete().getBagagens().contains(bagagem));
	}	
	
	public void verify(Bagagem bagagem, MassaTestesBagagemEnum enumm){
		super.verify(bagagem, enumm);
		
		//Verifica pontos base
		verify(bagagem);
		
		//Verifica atributos comuns
		Assert.assertEquals(bagagem.getPeso(), 				enumm.getPeso(), 0);
		Assert.assertEquals(bagagem.getTipoBagagemEnum(),	enumm.getTipoBagagemEnum());
	}	
	
	public void verify(Bagagem bagagemBD, Bagagem bagagem){
		super.verify(bagagemBD, bagagem);
		
		//Verifica pontos base
		verify(bagagem);
		verify(bagagemBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(bagagemBD.getPeso(), 						bagagem.getPeso(), 0);
		Assert.assertEquals(bagagemBD.getTipoBagagemEnum(),				bagagem.getTipoBagagemEnum());
		
		BilheteVerificator.getInstance().verify(bagagemBD.getBilhete(),	bagagem.getBilhete());
	}
}
