package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class CiaAereaVerificator extends Verificator<CiaAerea, MassaTestesCiaAereaEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static CiaAereaVerificator instance = new CiaAereaVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private CiaAereaVerificator() {	
	}
	
	public static CiaAereaVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	public void verify(CiaAerea ciaAerea){
		super.verify(ciaAerea);
		
		Assert.assertTrue(StringUtils.isNotBlank(ciaAerea.getNome()));
	}	
	
	public void verify(CiaAerea ciaAerea, MassaTestesCiaAereaEnum ciaAereaEnum){
		super.verify(ciaAerea, ciaAereaEnum);
		
		//Verifica pontos base
		verify(ciaAerea);
		
		//Verifica atributos comuns
		Assert.assertEquals(ciaAerea.getNome(), ciaAereaEnum.getNome());
	}	
	
	public void verify(CiaAerea ciaAereaBD, CiaAerea ciaAerea){
		super.verify(ciaAereaBD, ciaAerea);
		
		//Verifica pontos base
		verify(ciaAerea);
		verify(ciaAereaBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(ciaAereaBD.getNome(), 		ciaAerea.getNome());
	}
}
