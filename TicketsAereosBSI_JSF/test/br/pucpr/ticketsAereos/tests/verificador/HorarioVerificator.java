package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Bilhete;
import br.pucpr.ticketsAereos.model.Horario;
import br.pucpr.ticketsAereos.model.enums.TipoBilheteEnum;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesHorarioEnum;
import br.pucpr.ticketsAereos.utils.DateUtils;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class HorarioVerificator extends Verificator<Horario, MassaTestesHorarioEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static HorarioVerificator instance = new HorarioVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private HorarioVerificator() {
	}

	public static HorarioVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
		
	public void verify(Horario horario){
		super.verify(horario);
		
		Assert.assertTrue(StringUtils.isNotBlank(horario.getCodigo()));
		
		Assert.assertNotNull(horario.getQtdEconomica());
		Assert.assertNotNull(horario.getQtdExecutiva());
		Assert.assertNotNull(horario.getQtdPrimeira());
		Assert.assertNotNull(horario.getChegada());
		Assert.assertNotNull(horario.getPartida());
		
		RotaVerificator.getInstance().verify(horario.getRota());
		
		//Verifica a associacao bidirecional com Rota
		Assert.assertTrue(horario.getRota().getHorarios().contains(horario));
		
		//Verifica se a lista de bilhetes contem a quantidade gerada
		Assert.assertEquals(horario.getEconomicas().size(), horario.getQtdEconomica());
		Assert.assertEquals(horario.getExecutivas().size(), horario.getQtdExecutiva());
		Assert.assertEquals(horario.getPrimeiras().size(), horario.getQtdPrimeira());
		
		//Verifica a associacao bidirecional com bilhete
		for(Bilhete bilhete : horario.getEconomicas()){
			Assert.assertEquals(bilhete.getHorario(), horario);
			Assert.assertTrue(TipoBilheteEnum.ECONOMICA.equals(bilhete.getTipoBilheteEnum()));
		}
			
		//Verifica a associacao bidirecional com bilhete
		for(Bilhete bilhete : horario.getExecutivas()){
			Assert.assertEquals(bilhete.getHorario(), horario);
			Assert.assertTrue(TipoBilheteEnum.EXECUTIVA.equals(bilhete.getTipoBilheteEnum()));
		}

		//Verifica a associacao bidirecional com bilhete
		for(Bilhete bilhete : horario.getPrimeiras()){
			Assert.assertEquals(bilhete.getHorario(), horario);
			Assert.assertTrue(TipoBilheteEnum.PRIMEIRA.equals(bilhete.getTipoBilheteEnum()));
		}
	}	
	
	public void verify(Horario horario, MassaTestesHorarioEnum horarioEnum){
		super.verify(horario, horarioEnum);
		
		//Verifica pontos base
		verify(horario);
		
		//Verifica atributos comuns
		Assert.assertEquals(horario.getCodigo(), 							horarioEnum.getCodigo());
		Assert.assertEquals(DateUtils.minimizeDate(horario.getChegada()),	DateUtils.minimizeDate(horarioEnum.getDataChegada()));
		Assert.assertEquals(DateUtils.minimizeDate(horario.getPartida()), 	DateUtils.minimizeDate(horarioEnum.getDataPartida()));
		Assert.assertEquals(horario.getQtdEconomica(), 						horarioEnum.getQtdEconomica());
		Assert.assertEquals(horario.getQtdExecutiva(), 						horarioEnum.getQtdExecutiva());
		Assert.assertEquals(horario.getQtdPrimeira(), 						horarioEnum.getQtdPrimeira());
	}	
	
	public void verify(Horario horarioBD, Horario horario){
		super.verify(horarioBD, horario);
		
		//Verifica pontos base
		verify(horario);
		verify(horarioBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(horarioBD.getCodigo(), 								horario.getCodigo());
		Assert.assertEquals(DateUtils.minimizeDate(horarioBD.getChegada()),	 	DateUtils.minimizeDate(horario.getChegada()));
		Assert.assertEquals(DateUtils.minimizeDate(horarioBD.getPartida()), 	DateUtils.minimizeDate(horario.getPartida()));
		Assert.assertEquals(horarioBD.getQtdEconomica(), 						horario.getQtdEconomica());
		Assert.assertEquals(horarioBD.getQtdExecutiva(), 						horario.getQtdExecutiva());
		Assert.assertEquals(horarioBD.getQtdPrimeira(), 						horario.getQtdPrimeira());
		
		RotaVerificator.getInstance().verify(horarioBD.getRota(), horario.getRota());
	}
}
