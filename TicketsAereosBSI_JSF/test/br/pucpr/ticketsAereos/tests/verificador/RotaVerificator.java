package br.pucpr.ticketsAereos.tests.verificador;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import br.pucpr.ticketsAereos.model.Rota;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesRotaEnum;

/**
 * Classe responsavel por realizar comparacoes entre as classes
 * @author Mauda
 *
 */
public class RotaVerificator extends Verificator<Rota, MassaTestesRotaEnum>{
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////

	private static RotaVerificator instance = new RotaVerificator();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////

	private RotaVerificator() {
	}
	
	public static RotaVerificator getInstance() {
		return instance;
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////

	public void verify(Rota rota){
		super.verify(rota);
		
		Assert.assertTrue(StringUtils.isNotBlank(rota.getNome()));
		Assert.assertTrue(StringUtils.isNotBlank(rota.getDescricao()));
		
		CiaAereaVerificator.getInstance().verify(rota.getCiaAerea());
		AeroportoVerificator.getInstance().verify(rota.getOrigem());
		AeroportoVerificator.getInstance().verify(rota.getDestino());
		
		//Verifica a associacao bidirecional com CiaAerea
		Assert.assertTrue(rota.getCiaAerea().getRotas().contains(rota));
	}	
	
	public void verify(Rota rota, MassaTestesRotaEnum ciaAereaEnum){
		super.verify(rota, ciaAereaEnum);
		
		//Verifica pontos base
		verify(rota);
		
		//Verifica atributos comuns
		Assert.assertEquals(rota.getNome(), 							ciaAereaEnum.getNome());
		Assert.assertEquals(rota.getDescricao(), 						ciaAereaEnum.getDescricao());
	}	
	
	public void verify(Rota rotaBD, Rota rota){
		super.verify(rotaBD, rota);
		
		//Verifica pontos base
		verify(rota);
		verify(rotaBD);
		
		//Verifica atributos comuns
		Assert.assertEquals(rotaBD.getNome(), 							rota.getNome());
		Assert.assertEquals(rotaBD.getDescricao(), 						rota.getDescricao());
		
		CiaAereaVerificator.getInstance().verify(rotaBD.getCiaAerea(), 	rota.getCiaAerea());
		AeroportoVerificator.getInstance().verify(rotaBD.getOrigem(), 	rota.getOrigem());
		AeroportoVerificator.getInstance().verify(rotaBD.getDestino(), 	rota.getDestino());
	}
}
