--Rodar primeiro somente essa linha
CREATE SEQUENCE SEQ_ID_PESSOA AS BIGINT START WITH 1 INCREMENT BY 1;

--Agora rodar todo o resto
CREATE TABLE TB_CIA_AEREA(
	ID					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	NOME 				VARCHAR(50) NOT NULL);
	
CREATE TABLE TB_ENDERECO(
	ID					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	RUA 				VARCHAR(50) NOT NULL,
	NUMERO 				INTEGER NOT NULL,
	COMPLEMENTO	 		VARCHAR(50) NOT NULL,
	BAIRRO 				VARCHAR(50) NOT NULL,
	CIDADE 				VARCHAR(50) NOT NULL,
	ESTADO 				VARCHAR(50) NOT NULL,
	PAIS 				VARCHAR(50) NOT NULL);
	
CREATE TABLE TB_AEROPORTO(
	ID 					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_ENDERECO 		BIGINT NOT NULL,
	NOME 				VARCHAR(50) NOT NULL,
	CODIGO	 			VARCHAR(50) NOT NULL,
	CONSTRAINT	 		FK_TB_AEROPORTO_1 FOREIGN KEY(ID_ENDERECO) REFERENCES TB_ENDERECO(ID));

CREATE TABLE TB_ROTA(
	ID					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_CIA_AEREA			BIGINT NOT NULL,
	ID_ORIGEM			BIGINT NOT NULL,
	ID_DESTINO			BIGINT NOT NULL,
	NOME 				VARCHAR(50) NOT NULL,
	DESCRICAO			VARCHAR(200) NOT NULL,
	CONSTRAINT 			FK_TB_ROTA_1 FOREIGN KEY(ID_CIA_AEREA) REFERENCES TB_CIA_AEREA(ID),
	CONSTRAINT 			FK_TB_ROTA_2 FOREIGN KEY(ID_ORIGEM) REFERENCES TB_AEROPORTO(ID),
	CONSTRAINT 			FK_TB_ROTA_3 FOREIGN KEY(ID_DESTINO) REFERENCES TB_AEROPORTO(ID));

CREATE TABLE TB_FUNCIONARIO(
	ID 					BIGINT GENERATED BY DEFAULT AS SEQUENCE SEQ_ID_PESSOA NOT NULL PRIMARY KEY,
	ID_ENDERECO 		BIGINT NOT NULL,
	ID_CIA_AEREA		BIGINT NOT NULL,
	NOME 				VARCHAR(50) NOT NULL,
	EMAIL 				VARCHAR(50) NOT NULL,
	TELEFONE 			VARCHAR(15) NOT NULL,
	DATA_NASCIMENTO 	DATE NOT NULL,
	CODIGO	 			VARCHAR(20) NOT NULL,
	CONTA_CORRENTE		VARCHAR(15) NOT NULL,
	CONSTRAINT 			FK_TB_FUNCIONARIO_1 FOREIGN KEY(ID_ENDERECO) REFERENCES TB_ENDERECO(ID),
	CONSTRAINT 			FK_TB_FUNCIONARIO_2 FOREIGN KEY(ID_CIA_AEREA) REFERENCES TB_CIA_AEREA(ID));	
	
	
CREATE TABLE TB_PASSAGEIRO(
	ID 					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_ENDERECO 		BIGINT NOT NULL,
	NOME 				VARCHAR(50) NOT NULL,
	EMAIL 				VARCHAR(50) NOT NULL,
	TELEFONE 			VARCHAR(15) NOT NULL,
	DATA_NASCIMENTO		DATE NOT NULL,
	NUMERO_CARTAO		VARCHAR(20) NOT NULL,
	DOCUMENTO			VARCHAR(15) NOT NULL,
	CONSTRAINT 			FK_TB_PASSAGEIRO_1 FOREIGN KEY(ID_ENDERECO) REFERENCES TB_ENDERECO(ID));
	
CREATE TABLE TB_HORARIO(
	ID 					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_ROTA				BIGINT NOT NULL,
	PARTIDA		 		DATE NOT NULL,
	CHEGADA		 		DATE NOT NULL,
	CODIGO				VARCHAR(20) NOT NULL,
	QTD_ECONOMICA		INTEGER NOT NULL,
	QTD_EXECUTIVA		INTEGER NOT NULL,
	QTD_PRIMEIRA		INTEGER NOT NULL,
	CONSTRAINT 			FK_TB_HORARIO_1 FOREIGN KEY(ID_ROTA)  REFERENCES TB_ROTA(ID));

CREATE TABLE TB_BILHETE(
	ID 					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_PASSAGEIRO		BIGINT,
	ID_HORARIO			BIGINT NOT NULL,
	ASSENTO				VARCHAR(10),
	TIPO				VARCHAR(10) NOT NULL,
	SITUACAO			VARCHAR(10) NOT NULL,
	CONSTRAINT 			FK_TB_BILHETE_1 FOREIGN KEY(ID_PASSAGEIRO) REFERENCES TB_PASSAGEIRO(ID),
	CONSTRAINT 			FK_TB_BILHETE_2 FOREIGN KEY(ID_HORARIO)  REFERENCES TB_HORARIO(ID));


CREATE TABLE TB_BAGAGEM(
	ID 					BIGINT GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,
	ID_BILHETE			BIGINT NOT NULL,
	PESO				DOUBLE NOT NULL,
	TIPO				VARCHAR(10) NOT NULL,
	CONSTRAINT 			FK_TB_BAGAGEM_1 FOREIGN KEY(ID_BILHETE) REFERENCES TB_BILHETE(ID));